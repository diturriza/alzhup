module.exports = {
  attributes: {
    personTagId: {
      model: 'Tag'
    },
    topLeft: {
      type: 'array'
    },
    bottomRight: {
      type: 'array'
    },
    memoryId: {
      model : 'Memory'
    }
  }
};