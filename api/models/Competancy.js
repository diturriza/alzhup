module.exports = {
  attributes: {
  	category: {
  		type: 'string',
  	},
    text: {
      type: 'string',
      required: true
    }
  }
};