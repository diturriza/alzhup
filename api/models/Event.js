module.exports = {

  attributes: {

    contactId: {
      model: 'Contact'
    },
    userId: {
      model: 'User'
    },
    eventTime: {
      type: 'date'
    },
    eventType: {
      type: 'string'
      // ,enum : [] Not set yet
    },
    title: {
      type: 'string'
    },
    details: {
      type: 'string'
    }
  }
};