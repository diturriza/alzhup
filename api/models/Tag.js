module.exports = {

  attributes: {
    type: {
      type: 'string',
      enum: ['custom', 'person']
    },
    name: {
      type: 'string'
    },
    contactId: {
      model: 'Contact'
    },
    relationship: {
      type: 'string'
    },
    active: {
      type: 'boolean'
    },
    userId: {
      model: 'User'
    },
    memoryId: {
      model : 'Memory'
    }
  }
};