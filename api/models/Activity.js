module.exports = {

  attributes: {
    gameId: {
      type: 'integer'
    },
    memoryBanks: {
      model: 'MemoryBank'
    },
    competancies: {
      model: 'Competancy'
    },
    area: {
      type: 'string'
    },
    funtion: {
      type: 'string'
    },
    title: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    recommendedTime: {
      type: 'integer'
    },
    difficulty: {
      type: 'integer'
    },
    externals: {
      type: 'string'
    },
    variations: {
      type: 'string'
    },
    personalization: {
      type: 'string'
    }
  }
};