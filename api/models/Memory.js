module.exports = {

  attributes: {
    resourceURL: {
      type: 'string'
    },
    resourceType: {
      type: 'string',
      enum: ['photo', 'audio', 'video']
    },
    details: {
      type: 'string'
    },
    date: {
      type: 'Date'
    },
    memoryBanks: {
      collection: 'MemoryBank',
      via: 'memoryId'
    },
    peopleTags: {
      collection: 'tag',
      via: 'memoryId'
    },
    customTags: {
      collection: 'tag',
      via: 'memoryId'
    },
    faces: {
      collection: 'face',
      via: 'memoryId'
    },
    meta: {
      type: 'json' // Object (MemoryMeta)
    },
    user: {
      model: 'User'
    }
  }
};