var bcrypt = require('bcryptjs');

module.exports = {
  attributes: {
    firstname: {
      type: 'string',
      required: false
    },
    lastname: {
      type: 'string',
      required: false
    },
    username: {
      type: 'string',
      required: true,
      unique: true
    },
    password: {
      type: 'string',
      required: true
    },
    isAdmin: {
      type: 'boolean',
      defaultsTo: false
    },
    role: {
      model: 'Role'
    },

    //Override toJSON method to remove password from API
    toJSON: function() {
      var obj = this.toObject();
      // Remove the password object value
      delete obj.password;
      // return the new object without password
      return obj;
    }
  },
  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
        // log(err, hash);
        if (err) {
          return cb(err);
        } else {
          user.password = hash;
          cb();
        }
      });
    });
  }
};