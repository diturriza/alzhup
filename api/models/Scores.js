module.exports = {

  attributes: {
    competancy: {
      model: 'Competancy'
    },
    score: {
      type: 'integer'
    }
  }
};