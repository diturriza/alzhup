module.exports = {

  attributes: {
    senderContactId: {
      model: 'contact'
    },
    userId: {
      model: 'User'
    },
    message: {
      type: 'string'
    },
    title: {
      type: 'string'
    },
    unread: {
      type: 'boolean'
    }
  }
};