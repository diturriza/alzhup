module.exports = {

  attributes: {
    userId: {
      model: 'User'
    },
    startTime: {
      type: 'date'
    },
    endTime: {
      type: 'date'
    },
    complete: {
      type: 'boolean'
    },
    results: {
      type: 'array' // [SessionResults]
    },
    competancyAgregate: {
      type: 'array' // [Scores]
    }
  }
};