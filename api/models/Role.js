module.exports = {
  attributes: {
    name: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    isActive: {
      type: 'string'
    }
  },
  makeDefaults: function(cb) {
    Role.findOrCreate({
      name: "Admin"
    }, {
      name: "Admin",
      description: "TheBoss",
      isActive: true
    }).exec(cb);
  }
}