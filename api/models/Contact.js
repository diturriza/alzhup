module.exports = {

  attributes: {

    name: {
      type: 'string'
    },
    phone: {
      type: 'integer'
    },
    address: {
      type: 'string'
    },
    location: {
      type: 'array'
    },
    relationship: {
      type: 'string'
    },
    isProvider: {
      type: 'boolean',
      defaultsTo: false
    },
    isCaregiver: {
      type: 'boolean',
      defaultsTo: false
    },
    isUser: {
      type: 'boolean',
      defaultsTo: false
    },
    details: {
      type: 'string'
    },
    photoURL: {
      type: 'string'
    },
    userId: {
      model: 'User'
    }
  }
};