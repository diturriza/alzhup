var passport = require('passport'),
    request  = require('request'),
    moment = require('moment'),
    sha1 = require('sha1'),
    appCreds = require('../../config/authentication').authentication.app;
/**
 * OauthController
 *
 * @module controller
 * @class oauthController
 * @namespace sails.controllers
 */

module.exports = {
  /**
   * ask to login before give access to application to see your profile
   *
   * @method oauthController
   * @param {Request} req Express request object
   * should contain the following query string
   *
   * next : redirect to next url after login successfully
   */
  // GET /login
  login: function (req, res) {
    var nextUrl = req.query.next ? req.query.next : '/';
    res.view({ nextUrl: nextUrl });
  },
  // POST /login/client - xauth
  processClient: function (req, res) {
    var url = 'http://localhost:1337/oauth/access_token';
    var params = {
      oauth_consumer_key: req.body.key,
      oauth_nonce: nonce(32),
      oauth_timestamp: timestamp(),
      oauth_signature_method: 'HMAC-SHA1',
      oauth_version:'1.0'
    };

    var msg = 'POST&' + encode(url) + '&' + encode(serialize(params));
    params.oauth_signature = sha1(msg);

    var auths = [];
    for ( var key in params ){
      var val = params[key];
      auths.push(key + '="' + val + '"');
    }

    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'OAuth ' + auths.sort().join(',')
    };

    var form = {
      username: req.body.username,
      password: req.body.password,
      grant_type: 'password',
      client_id: req.body.key,
      client_secret: appCreds.clientSecret
    };


    request.post({ url: url, form: form, headers: headers }, function (err, resp, body) {
      if (err) {
        return res.send(401, err);
      } else if (resp.statusCode == 401) {
        return res.send(400, body)
      } else {
        body = JSON.parse(body);
        var userData = {
          expirationDate: moment().add('days', 7).toDate(),
          username: form.username,
          accessToken: body.access_token
        };
        req.session[body.access_token] = userData;
        res.cookie('user', JSON.stringify(userData));
        return res.send(userData);
      }
    });
  },
  // POST oauth/access_token, from above, middleware does the magic
  processClientCallback: function( req, res ){
    res.end();
  },
  // POST /login - HTTP Auth - untested
  process: function(req, res){
    var user = req.params.all();
    passport.authenticate('local', function(err, user, info){
      if ((err) || (!user)) {
        return res.badRequest({"status": "error"});
      }
      req.logIn(user, function(err){
        if (err) { return res.badRequest({"status": "error"}); }
        res.ok({"status": "ok", "results": user});
      });
    })(req, res);
  },
  // POST logout
  logout: function(req,res){
    req.session.destroy(function (err) {
      res.redirect(200, '/'); //Inside a callback... bulletproof!
    });
  }
};

// from https://oauth.googlecode.com/svn/code/javascript/oauth.js

function nonce(length) {
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
  var result = "";
  for (var i = 0; i < length; ++i) {
    var rnum = Math.floor(Math.random() * chars.length);
    result += chars.substring(rnum, rnum + 1);
  }
  return result;
}

function timestamp() {
  var t = (new Date()).getTime();
  return Math.floor(t / 1000);
}

function encode(s) {
  if (s == null) {
    return "";
  }
  if (s instanceof Array) {
    var e = "";
    for (var i = 0; i < s.length; ++s) {
      if (e != "") e += '&';
      e += encode(s[i]);
    }
    return e;
  }
  s = encodeURIComponent(s);
  // Now replace the values which encodeURIComponent doesn't do
  // encodeURIComponent ignores: - _ . ! ~ * ' ( )
  // OAuth dictates the only ones you can ignore are: - _ . ~
  // Source: http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Functions:encodeURIComponent
  s = s.replace(/\!/g, "%21");
  s = s.replace(/\*/g, "%2A");
  s = s.replace(/\'/g, "%27");
  s = s.replace(/\(/g, "%28");
  s = s.replace(/\)/g, "%29");
  return s;
}

function serialize(obj) {
  var chunks = [];
  for (var key in obj) {
    var val = obj[key];
    chunks.push(encodeURIComponent(key) + "=" + encodeURIComponent(val));
  }
  return chunks.join("&");
}