/**
 * TagController
 *
 * @description :: Server-side logic for managing Tags
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {



  /**
   * `TagController.addCompetancy()`
   */
  addCompetancy: function(req, res) {
    Competancy.create(req.params.all(), function createdCompetancy(err, competancy) {
      if (err) 
        return res.badRequest(err);
      return res.ok(competancy);
    });
  },


  /**
   * `TagController.addMemoryBank()`
   */
  addMemoryBank: function(req, res) {
    MemoryBank.create(req.params.all(), function createdMemoryBank(err, memoryBank) {
      if (err) 
        return res.badRequest(err);
      return res.ok(memoryBank);
    });
  },


  /**
   * `TagController.getCompentancyList()`
   */
  getCompetancyList: function(req, res) {
    Competancy.find({}, function gotCompetancyList(err, competancyList) {
      if (err) 
        return res.badRequest(err);
      return res.ok(competancyList);
    });
  },


  /**
   * `TagController.getMemoryBankKList()`
   */
  getMemoryBankList: function(req, res) {
    MemoryBank.find({}, function gotMemoryBankKList(err, memoryBankKList) {
      if (err)
        return res.badRequest(err);
      return res.ok(memoryBankKList);
    });

  },


  /**
   * `TagController.getCustomTags()`
   */
  getCustomTags: function(req, res) {
    Tag.find({
      type: 'custom'
    }, function gotCustomTags(err, customTags) {
      if (err)
        return res.badRequest(err);
      return res.ok(customTags);
    });
  },


  /**
   * `TagController.getPeopleTags()`
   */
  getPeopleTags: function(req, res) {
    Tag.find({
      type: 'person'
    }, function gotpeopleTags(err, peopleTags) {
      if (err)
        return res.badRequest(err);
      return res.ok(peopleTags);
    });
  },


  /**
   * `TagController.addCustomTag()`
   */
  addCustomTag: function(req, res) {

    var customTag = req.params.all();
    customTag.type = 'custom';

    Tag.create(customTag, function createdCustomTag(err, customTag) {
      if (err)
        return res.badRequest(err);
      return res.ok(customTag);
    });
  },


  /**
   * `TagController.addPeopleTag()`
   */
  addPeopleTag: function(req, res) {

    var peopleTag = req.params.all();
    peopleTag.type = 'person';

    Tag.create(peopleTag, function createdCustomTag(err, peopleTag) {
      if (err)
        return res.badRequest(err);
      return res.ok(peopleTag);
    });
  },


  /**
   * `TagController.disableCustomTag()`
   */
  disableCustomTag: function(req, res) {
    Tag.destroy({
      id: req.param('id')
    }, function removedTag(err, tag) {
      if (err)
        return res.badRequest(err);
      if (!_.isEmpty(tag)) {
        return res.ok({
          "message": "Tag has been removed"
        });
      } else {
        return res.ok({
          "error": true,
          "message": "Tag not found"
        });
      }
    });
  },


  /**
   * `TagController.disablePeople()`
   */
  disablePeopleTag: function(req, res) {
    Tag.destroy({
      id: req.param('id')
    }, function removedTag(err, tag) {
      if (err)
        return res.badRequest(err);
      if (!_.isEmpty(tag)) {
        return res.ok({
          "message": "Tag has been removed"
        });
      } else {
        return res.ok({
          "error": true,
          "message": "Tag not found"
        });
      }
    });
  }
};