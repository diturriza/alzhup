/**
 * ActivityController
 *
 * @description :: Server-side logic for managing Activities
 */

module.exports = {



  /**
   * `ActivityController.createActivity()`
   */
  createActivity: function(req, res) {
    Activity.create(req.params.all(), function createdActivity(err, activity) {
      if (err) return res.badRequest(err);
      return res.ok(activity);
    });
  },


  /**
   * `ActivityController.updateActivity()`
   */
  updateActivity: function(req, res) {

    Activity.update({
      id: req.param('id')
    }, req.params.all(), function updatedActivity(err, activity) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(activity)) {
        return res.ok(activity);
      } else {
        var error = {
          error: true,
          message: "Activity not found"
        };
        return res.badRequest(error);
      }
    });

  },


  /**
   * `ActivityController.destroyActivity()`
   */
  destroyActivity: function(req, res) {
    Activity.destroy({
      id: req.param('id')
    }, function removedActivity(err, activity) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(activity)) {
        return res.ok({
          "message": "Activity has been removed"
        });
      } else {
        return res.badRequest({
          "error": true,
          "message": "Activity not found"
        });
      }
    });
  },


  /**
   * `ActivityController.getActivity()`
   */
  getActivity: function(req, res) {
    Activity.find({
      id: req.param('id')
    }, function gotActivity(err, activity) {
      if (err) return res.badRequest(err);
      return res.ok(activity);
    });
  },


  /**
   * `ActivityController.getNextActivity()`
   */
  getNextActivity: function(req, res) {

    // Get userId by session
    // var userId = req.session.user;

    // Mock userId
    var userId = '1234abcd5678';

    // Mock of ActivitySessionService - Just return true 
    ActivitySessionService.getNextActivity(userId, function(err, result) {
      if (err) return res.badRequest(err);
      if(result) return res.ok(f.activity);
    });

  },


  /**
   * `ActivityController.getActivityList()`
   */
  getActivityList: function(req, res) {
    Activity.find({}, function gotActivities(err, activities) {
      if (err) return res.badRequest(err);
      return res.ok(activities);
    });
  }

};