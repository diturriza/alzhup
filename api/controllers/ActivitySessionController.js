/**
 * ActivitySessionController
 *
 * @description :: Server-side logic for managing Activitysessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {


  /**
   * `ActivitySessionController.startActivitySession()`
   */
  startActivitySession: function(req, res) {

    // Get userId by session
    var userId = req.session.user;

    ActivitySession.create({
      userId: userId,
      startTime: new Date()
    }, function(err, activitySession) {
      if (err) return res.badRequest(err);
      return res.ok(activitySession);
    });
  },


  /**
   * `ActivitySessionController.endActivitySession()`
   */
  endActivitySession: function(req, res) {
    // Get userId by session
    var userId = req.session.user;

    ActivitySession.update({
      userId: userId
    }, {
      endTime: new Date()
    }, function(err, activitySession) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(activitySession)) {
        return res.ok(activitySession);
      } else {
        return res.badRequest({
          "error": true,
          "message": "Activity Session not found"
        });
      }
    });
  },


  /**
   * `ActivitySessionController.progressActivitySession()`
   */
  progressActivitySession: function(req, res) {
    var userId = req.session.user;

    ActivitySession.update({
      userId: userId
    }, {
      results: req.param('activitySessionResults')
    }, function(err, activitySession) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(activitySession)) {
        return res.ok({
          "message": "Activity Session was updated"
        });
      } else {
        return res.badRequest({
          "error": true,
          "message": "Activity Session not found"
        });
      }
    });
  }
};