/**
* Home controller to render home page
*
* @class homeController
* @namespace sails.controllers
* @module controllers
*/

module.exports = {
  /**
   * will render the home/index.ejs and send user cookie to determine if the user is logged in or not
   * for more information https://github.com/fnakstad/angular-client-side-auth/tree/master/server/controllers
   * @method index
   * @param {Request} req Express request object
   * @param {Response} res Express response object
  */
  /**
   * [index description]
   * @param  {[type]} req
   * @param  {[type]} res
   */
  index: function (req, res) {
    res.view();
  }
}

/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */