/**
* to login with 3-party (facebook,twitter,google,linkedin)
*
* Note: to change any apps configuration check api/services
* @class oauthExternalController
* @namespace sails.controllers
* @module controllers
*/
var passport = require('passport');

module.exports = {

  // setup in config/routes.js
  externalCallback: function(req, res) {
    var service = req.param('serviceName');
    passport.authenticate(service, {
      successRedirect: '/',
      failureRedirect: '/login'
    })
  },

  externalService: function(req, res){
    var service = req.param('serviceName');
    log(service);
    passport.authenticate(service, sails.config.authentication[service].options);
  }
};
