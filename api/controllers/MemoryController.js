/**
 * MemoryController
 *
 * @description :: Server-side logic for managing Memories
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  /**
   * `MemoryController.addMemory()`
   */
  addMemory: function(req, res) {    

    // Get userId by session
    var userId = req.session.user;

    var memory = req.params.all();
    memory.userId = userId;

    Memory.create(memory, function(err, memory) {
      if (err) return res.badRequest(err);
      return res.ok(memory);
    });
  },


  /**
   * `MemoryController.getMemory()`
   */
  getMemory: function(req, res) {
    Memory.findOne({
      id: req.param('id')
    }, function(err, memory) {
      if (err) return res.badRequest(err);
      return res.ok(memory);
    });
  },


  /**
   * `MemoryController.getMemoriesByTag()`
   */
  getMemoriesByTag: function(req, res) {

    MemoryService.populateMemories(req.param('tagType'), req.param('tagId'), function(err, result) {
      if (err) res.badRequest(err);
      if (!_.isEmpty(result))
        return res.ok(result)
      else
        return res.ok({
          message: "Memories not found"
        });
    });

  },


  /**
   * `MemoryController.getMemoriesByBank()`
   */
  getMemoriesByBank: function(req, res) {

    Memory.find(req.param('bankId'))
      .populate('memoryBanks')
      .exec(function(err, memories) {
        if (err) res.badRequest(err);
        if (!_.isEmpty(memories))
          return res.ok(memories)
        else
          return res.ok({
            message: "Memories not found"
          });
      });

  },


  /**
   * `MemoryController.listMemoriesByDate()`
   */
  listMemoriesByDate: function(req, res) {
    Memory.find({
      eventTime: {
        '>=': req.param('startDate')
      }
    })
      .limit(req.param('limit'))
      .skip(req.param('skip'))
      .exec(function(err, memories) {
        if (err) return res.badRequest(err);
        return res.ok(memories);
      });
  },


  /**
   * `MemoryController.uploadMemory()`
   */
  uploadMemory: function(req, res) {
    MemoryService.uploadFile(req.param('id'), req.file('uploadFile'), function (err, result) {
      if(err) return res.badRequest(err);
      return res.ok(result);
    });
  },


  /**
   * `MemoryController.updateMemoryDetails()`
   */
  updateMemoryDetails: function(req, res) {
    Memory.update({
      id: req.param('id')
    }, {
      details: req.param('details')
    }, function updatedMemoryDetails(err, memory) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(memory))
        return res.ok(memory)
      else
        return res.ok({
          message: "Memory not found"
        });
    });
  },


  /**
   * `MemoryController.updateMemoryDate()`
   */
  updateMemoryDate: function(req, res) {
    Memory.update({
      id: req.param('id')
    }, {
      date: new Date(req.param('date'))
    }, function updatedMemoryDate(err, memory) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(memory))
        return res.ok(memory)
      else
        return res.ok({
          message: "Memory not found"
        });
    });
  },


  /**
   * `MemoryController.linkTag()`
   */
  linkTag: function(req, res) {
    Tag.update({
      id: req.param('tagId')
    }, {
      memoryId: req.param('id')
    }, function(err, tag) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(tag))
        return res.ok(tag)
      else
        return res.ok({
          message: "Tag not found"
        });
    });
  },


  /**
   * `MemoryController.unlinkTag()`
   */
  unlinkTag: function(req, res) {
    Tag.destroy({
      id: req.param('tagId')
    }, function(err, tag) {
      if (err) return res.badRequest(err);
      return res.ok(tag)
    });
  },


  /**
   * `MemoryController.linkFace()`
   */
  linkFace: function(req, res) {
    Face.create({
      id: req.param('tagId'),
      personTagId: req.param('personTagId'),
      topLeft: req.param('topLeft'),
      bottomRight: req.param('bottomRight'),
      memoryId: req.param('id')
    }, function(err, face) {
      if (err) return res.badRequest(err);
      return res.ok(face);
    });
  },


  /**
   * `MemoryController.unlinkFace()`
   */
  unlinkFace: function(req, res) {
    Face.update({
      id: req.param('personTagId'),
      memoryId: req.param('id')
    }, {
      memoryId: ''
    }, function(err, face) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(face))
        return res.ok(face)
      else
        return res.ok({
          message: "PersonTag not found"
        });
    });
  },


  /**
   * `MemoryController.deleteMemory()`
   */
  deleteMemory: function(req, res) {
    Memory.destroy({
      id: req.param('id')
    }, function deletedMemory(err, memory) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(memory)) {
        return res.ok({
          "message": "Memory has been removed"
        });
      } else {
        return res.badRequest({
          "error": true,
          "message": "Memory not found"
        });
      }
    });
  },


  /**
   * `MemoryController.getFeaturedMemory()`
   */
  getFeaturedMemory: function(req, res) {
    MemoryService.getFeaturedMemory(function(err, result) {
      return res.ok({
        message: "Featured Memory not found"
      });
    });
  },

  /**
   * `MemoryController.getMemoryUpdates()`
   */
  getMemoryUpdates: function(req, res) {
    Memory.find({}, function(err, memories) {
      if (err) return res.badRequest(err);
      return res.ok(memories);
    });
  }
};