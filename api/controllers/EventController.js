/**
 * EventController
 *
 * @description :: Server-side logic for managing Events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {



  /**
   * `EventController.addEvent()`
   */
  addEvent: function(req, res) {
    Event.create(req.params.all(), function(err, event) {
      if (err) return res.badRequest(err);
      return res.ok(event);
    });
  },


  /**
   * `EventController.updateEvent()`
   */
  updateEvent: function(req, res) {
    Event.update({
      id: req.param('id')
    }, req.params.all(), function(err, event) {
      if (err) return res.badRequest(err);
      return res.ok(event);
    });
  },


  /**
   * `EventController.getEvent()`
   */
  getEvent: function(req, res) {
    Event.findOneById(req.param('id'), function(err, event) {
      if (err) return res.badRequest(err);
      return res.ok(event);
    });
  },


  /**
   * `EventController.getEventList()`
   */
  getEventList: function(req, res) {
    var startDate = new Date(req.param('startDate'));
    var numDays = Number(req.param('numDays'));
    var endDate = startDate;
    endDate.setDate(startDate.getDate() + numDays);
    Event.find()
      .where({
        eventTime: {
          '>=': startDate
        },
      })
      .where({
        eventTime: {
          '<=': endDate
        }
      })
      .exec(function(err, events) {
        if (err) return res.badRequest(err);
        return res.ok(events);
      });
  },

  /**
   * `EventController.getEventsForContact()`
   */
  getEventsForContact: function(req, res) {
    Event.findOne({
      contactId: req.param('contactId')
    }, function(err, events) {
      if (err) return res.badRequest(err);
      return res.ok(events);
    });
  },


  /**
   * `EventController.removeEvent()`
   */
  removeEvent: function(req, res) {
    Event.destroy({
      id: req.param('id')
    }, function(err, event) {
      if (err) return res.badRequest(err);
      if (!_.isEmpty(event)) {
        return res.ok({
          "message": "Event has been removed"
        });
      } else {
        return res.badRequest({
          "error": true,
          "message": "Event not found"
        });
      }
    });
  }
};