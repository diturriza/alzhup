'use strict';
/**
 * Allow any authenticated user.
 */
module.exports = function(req, res, ok) {

  // User is allowed, proceed to controller
  if (!_.isEmpty(req.session.passport.user)) {
    User.findOne({
      id: req.session.passport.user
    }, function(err, user) {
      if (err) return res.badRequest(err);
      if (user.isAdmin)
        ok();
      else
        return res.forbidden('You are not Admin');
    });
  } else {
    res.forbidden('You are not Admin');
  }
};