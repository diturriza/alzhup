/**
* A service for handle subscriptions and transactions with Braintree
* PaymentGateway is initialized on Bootstrap
*
* @author Edison Galindo <edison@rokk3rlabs.com>
* @TODO Transactions functions
*/
module.exports = {
  customerCreate: customerCreate,
  subscription: subscription,
  subscriptionDetails: subscriptionDetails,
  transactionRefund: transactionRefund
}
/**
* @param {Object} {firstName,lastName,paymentMethodNonce}
* @author Edison Galindo <edison@rokk3rlabs.com>
*/
function customerCreate(customerRequest, next ){
  paymentGateway.customer.create(customerRequest, function (err, result) {
    if (err) return next("Error: " + result.message);
    next(null, result);
  });
}
/**
* @param {string} customerId
* @param {string} planId
* @author Edison Galindo <edison@rokk3rlabs.com>
*/
function subscription(customerId, planId, next){
  async.waterfall([
    function customerFind(callback){
      paymentGateway.customer.find(customerId, function (err, customer) {
        if (err) callback("No customer found for id: " + customerId);
        else callback(null, customer);
      });
    },
    function subscriptionCreate(customer, callback){
      var subscriptionRequest = {
        paymentMethodToken: customer.creditCards[0].token,
        planId: planId
      };
      paymentGateway.subscription.create(subscriptionRequest, function (err, result) {
        if (err) callback("Error creating subscription");
        callback(null, result.subscription);
      });
    }
  ], function (err, subscription) {
     if(err) return next(err);
     next(null, subscription);
  });
}
/**
* Gets the details of a subscription
* @param {string} subscriptionId
* @callback {Object}
* @author Edison Galindo <edison@rokk3rlabs.com>
*/
function subscriptionDetails(subscriptionId, next){
  paymentGateway.subscription.find(subscriptionId, function (err, result) {
    if(err) return next(err)
    next(null, result)
  });
}
/**
* Makes a request for refund a transaction
* @param {string} transactionId
*/
function transactionRefund(transactionId, next){
  paymentGateway.transaction.refund(transactionId, function (err, result) {
    if(err) return next(err);
    next(null, result);
  });
}