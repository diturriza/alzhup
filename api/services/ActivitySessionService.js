module.exports = {
  getNextActivity: getNextActivity
}

function getNextActivity (userId, cb) {
	if(!_.isEmpty(userId))
  	cb(null, true);
  else
  	cb({error:true, message:"userId not found"}); 
}