module.exports = {
  populateMemories: populateMemories
}

function populateMemories (tagId, tagType, cb) {

  Memory.find({}, function findMemories(err, memories){
    if(err) cb(err);
    cb(null, memories);
  });
}