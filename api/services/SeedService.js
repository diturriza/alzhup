'use strict';

var Chance = require('chance');
var chance = new Chance(Math.random);

module.exports = {
  makeMemories: makeMemories,
  makePeople: makePeople,
  makeCustomtags: makeCustomtags,
  makeMemoryBanks: makeMemoryBanks,
  makeCompetancies: makeCompetancies,
  makeAll: makeAll
};

/**
 * Create 10 random memories if count is undefined.
 * @author Daniel Iturriza <diturriza17@gmail.com>
 * @method makeMemories
 * @param {int} count
 * @return
 */
function makeMemories(count, callback) {

  var count = count || 10;

  var memories = [];
  var memory = {};
  var face = {};
  var faces = [];
  var userId = '';

  async.waterfall([

      function(cb) {
        User.findOne({
          username: 'admin'
        }, function(err, user) {
          if (err) cb(err);
          cb(null, user.id);
        });
      },
      function(userId, cb) {
        Tag.find({
          type: 'person'
        }, function(err, tags) {
          if (err) cb(err);
          cb(null, userId, tags);
        })
      },
      function(userId, tags, cb) {
        for (var i = count - 1; i >= 0; i--) {
          memory.details = chance.sentence();
          memory.date = chance.date();
          memory.user = userId;

          face.personTagId = tags[i].id;
          face.topLeft = [chance.natural(), chance.natural()];
          face.bottomRight = [chance.natural(), chance.natural()];
          memories.push(memory);
          faces.push(face);
        }
        cb(null, memories, faces);
      },
      function(memories, faces, cb) {
        Memory.create(memories, function(err, memories) {
          if (err) cb(err);
          cb(null, memories, faces);
        });
      },
      function(memories, faces, cb) {
        var i = 0;
        _.forEach(memories, function(memory) {
          var request = require('supertest');
          request = request('http://localhost:1337');
          request.post('/memory/' + memory.id + '/upload')
            .set('Accept', 'application/json')
            .attach('uploadFile', './test/files/alzhup.png')
            .end(function(err, res) {
              if (err) cb(err);
            });
          faces[i].memoryId = memory.id;
        });
        cb(null, memories, faces);
      },
      function(memories, faces, cb) {
        Face.create(faces, function(err, faces) {
          if (err) cb(err);
          log.info(faces);
          cb(null, memories, faces);
        });
      }
    ],
    function(err, memories, faces) {
      if (err) return callback(err);
      log.verbose('made memories and tags');
      log.verbose(memories);
      log.verbose(faces);
      callback(null, {
        ok: true
      });
    });
}

/**
 * Create 10 contacts and their peopleTags if count is undefined.
 * @author Daniel Iturriza <diturriza17@gmail.com>
 * @method makePeople
 * @param {int} count
 * @return
 */
function makePeople(count, callback) {

  var count = count || 10;

  var people = [];
  var tags = [];
  var userId = '';

  async.waterfall([

    function(cb) {
      User.findOne({
        username: 'admin'
      }, function(err, user) {
        if (err) return err;
        cb(null, user.id);
      });
    },
    function(userId, cb) {
      for (var i = count - 1; i >= 0; i--) {
        var bool = chance.bool();
        var contactRandom = {
          name: chance.name(),
          phone: chance.natural(),
          address: chance.address(),
          location: [chance.longitude(), chance.latitude()],
          relationship: chance.sentence(),
          isCaregiver: bool,
          isProvider: !bool,
          isUser: false,
          details: chance.sentence(),
          photoURL: chance.url({
            extensions: ['jpg', 'png']
          })
        };

        var peopleTagRandom = {
          type: 'person',
          name: chance.word(),
          relationship: chance.sentence(),
          active: true,
          userId: userId
        };

        people.push(contactRandom);
        tags.push(peopleTagRandom);
      }
      cb(null, people, tags);
    },
    function(people, tags, cb) {
      Contact.create(people, function(err, contacts) {
        if (err) cb(err);
        cb(null, contacts, tags)
      });
    },
    function(contacts, tags, cb) {
      var i = 0;
      _.forEach(contacts, function(contact) {
        tags[i].contactId = contact.id;
        i++;
      });
      cb(null, contacts, tags);
    },
    function(contacts, tags, cb) {
      Tag.create(tags, function(err, tags) {
        if (err) cb(err);
        cb(null, contacts, tags);
      });
    }
  ], function(err, contacts, tags) {
    if (err) return callback(err);
    log.verbose('made Contacts and tags');
    log.verbose(contacts);
    log.verbose(tags);
    callback(null, {
      ok: true
    });
  });
}

/**
 * Create 10 custom Tags.
 * @author Daniel Iturriza <diturriza17@gmail.com>
 * @method makeCustomtags
 * @return
 */
function makeCustomtags(callback) {

  var userId = '';

  User.findOne({
    username: 'admin'
  }, function(err, user) {
    if (err) return err;
    userId = user.id;
  });
  var customTags = [];
  for (var i = 10 - 1; i >= 0; i--) {
    var customTagRandom = {
      type: 'custom',
      name: chance.word(),
      active: true,
      userId: userId
    };
    customTags.push(customTagRandom);
  }
  Tag.create(customTags, function(err, tag) {
    if (err) return callback(err);
    log.verbose('made custom tags');
    log.verbose(tag);
    callback(null, {
      ok: true
    });
  });
}

/**
 * Create List of memory banks from test/fixtures/memoryBanks.js
 * @author Daniel Iturriza <diturriza17@gmail.com>
 * @method makeMemoryBanks
 * @return
 */
function makeMemoryBanks(callback) {
  MemoryBank.create(f.memoryBanks, function(err, m) {
    if (err) return callback(err);
    log.verbose('made memory banks');
    log.verbose(m);
    callback(null, {
      ok: true
    });
  });
}

/**
 * Create List of competancies from test/fixtures/competancies.js
 * @author Daniel Iturriza <diturriza17@gmail.com>
 * @method makeCompetancies
 * @return
 */
function makeCompetancies(callback) {

  Competancy.create(f.competancies, function(err, c) {
    if (err) return callback(err);
    log.verbose('made competancies');
    log.verbose(c);
    callback(null, {
      ok: true
    });
  });
}

/**
 * Create memories, tags, people, faces and banks with one command makeAll()
 * @method makeAll
 * @return
 */
function makeAll() {
  async.waterfall([

    function(cb) {
      makeMemoryBanks(function(err, result) {
        if (err) cb(err);
        result.ok ? cb() : cb(result.ok);
      });
    },
    function(cb) {
      makeCompetancies(function(err, result) {
        if (err) cb(err);
        result.ok ? cb() : cb(result.ok);
      });
    },
    function(cb) {
      makePeople(null, function(err, result) {
        if (err) cb(err);
        result.ok ? cb() : cb(result.ok);
      });
    },
    function(cb) {
      makeCustomtags(function(err, result) {
        if (err) cb(err);
        result.ok ? cb() : cb(result.ok);
      });
    },
    function(cb) {
      makeMemories(null, function(err, result) {
        if (err) cb(err);
        result.ok ? cb() : cb(result.ok);
      });
    },

  ], function(err, results) {
    if (err) return err;
    log.verbose('success');
  });
}