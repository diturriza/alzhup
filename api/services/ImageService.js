'use strict';

var fs = require('fs');
var imageMagick = require('imagemagick');

/**
 * Create thumbnail for artist image
 * @author Juan Márquez <juanca@rokk3rlabs.com>
 * @param  {[file]} req [data of file]
 * @param  { Object } config.size=150  Size of thumbnail
 * @param  { fn } cb Callback
 * @return
 */

function createThumbnail(file, config, cb) {

  if ( _.isFunction(config) ){
    cb = config;
  }

  var min;
  var size = config.size || 150;
  var thumbName = file.fileName + 'Thumb.' + file.fileExt;

  imageMagick.identify(file.filePath, function(err, features) {
    if (err) cb(err);

    // find smallest side for squaring
    if (features.width >= features.height) {
      min = features.height;
    } else {
      min = features.width;
    }

    imageMagick.crop({
      srcPath: file.filePath,
      dstPath: sails.config.appConfig.uploadPath + thumbName,
      width: min,
      height: min,
      quality: 1,
      gravity: 'Center'
    }, function(err) {
      if (err) cb(err);

      imageMagick.resize({
        srcData: fs.readFileSync(file.serverPath + thumbName, 'binary'),
        width: size
      }, function(err, stdout) {
        if (err) cb(err);
        fs.writeFileSync(file.serverPath + thumbName, stdout, 'binary');
        cb(null, thumbName);
      });
    });
  });
}

module.exports = {
  createThumbnail : createThumbnail
};
