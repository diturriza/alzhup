module.exports = {
  populateMemories: populateMemories,
  getFeaturedMemory: getFeaturedMemory,
  uploadFile: uploadFile
}

function populateMemories(tagType, tagId, cb) {

  var type = tagType == 'custom' ? 'customTags' : 'peopleTags';

  Memory.find(tagId)
    .populate(type)
    .exec(function(err, memories) {
      if (err) cb(err);
      if (!_.isEmpty(memories))
        cb(null, memories)
      else
        return cb(null, {
          message: "Memories not found"
        });
    });
}

function getFeaturedMemory(cb) {
  cb(null, true);
}

function uploadFile(id, file, cb) {
  var uploadFile = file;
  var originFile = file._files[0].stream.filename;
  // var fileExtName = path.extname(originFile);
  //  fileExtName is to filter the extname (e.g. png,jpg, gif, ...)

  var uploadOptions = {
    dirname: '../../test/uploads',
    saveAs: function(file, cb) {
      cb(null, file.filename);
    },
    // maxBytes: 10 * 1000 * 1000  i.e: Max 10Mb 
  }

  async.waterfall([

    function(callback) {
      uploadFile.upload(uploadOptions, function(err, files) {
        if (err) callback(err)
        callback(null, files);
      });
    },
    function(files, callback) {
      Memory.update({
        id: id
      }, {
        resourceUrl: './test/uploads/' + files[0].filename, // For Local 
        resourceType: 'photo' // Just for test
      }, function(err, file) {
        if (err) callback(err);
        callback(null, {
          message: "Upload File Success!"
        });
      });
    }
  ], function(err, result) {
    if (err) cb(err);
    cb(null, result);
  });
}