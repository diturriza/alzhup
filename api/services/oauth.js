﻿//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and
//   profile), and invoke a callback with a user object.

var passport = require('passport'),
  bcrypt = require('bcryptjs'),
  LocalStrategy = require('passport-local').Strategy, // http
  BearerStrategy = require('passport-http-bearer').Strategy, // oauth2
  FacebookStrategy = require('passport-facebook').Strategy,
  GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
  LinkedInStrategy = require('passport-linkedin').Strategy,
  TwitterStrategy = require('passport-twitter').Strategy,
  authentication = require('../../config/authentication').authentication;

passport.use(new LocalStrategy(function(username, password, done) {
  User.findOne({
    username: username
  }).exec(function(err, user) {
    if (err) {
      return done(null, err);
    }
    if (!user) {
      return done(null, false, {
        message: 'Incorrect User'
      })
    }
    bcrypt.compare(password, user.password, function(err, res) {
      if (!res) {
        return done(null, false, {
          message: 'Invalid Password'
        })
      }
      return done(null, user);
    });
  });
}));

// see https://github.com/jaredhanson/passport-http-bearer/blob/master/examples/bearer/app.js#L23
// Supports OAuth2
// Use the BearerStrategy within Passport.
//   Strategies in Passport require a `validate` function, which accept
//   credentials (in this case, a token), and invoke a callback with a user
//   object.
passport.use(new BearerStrategy({
    passReqToCallback: true
  },
  function(req, token, done) {
    // asynchronous validation, for effect...
    process.nextTick(function() {
      // Find the user by token.  If there is no user with the given token, set
      // the user to `false` to indicate failure.  Otherwise, return the
      // authenticated `user`.  Note that in a production-ready application, one
      // would want to validate the token for authenticity.
      if (req.session[token] && moment().isBefore(req.session[token].expirationDate)) {
        req.session[token].expirationDate = moment().add('days', 7).toDate();
        return done(null, req.session[token]);
      } else {
        return done("invalid token");
      }
    });
  }
));

passport.use(new FacebookStrategy({
  clientID: authentication.facebook.clientID,
  clientSecret: authentication.facebook.clientSecret,
  callbackURL: authentication.facebook.callbackURL
}, function(accessToken, refreshToken, profile, done) {
  return done(null, accessToken, profile);
}));﻿

passport.use(new GoogleStrategy({
  clientID: authentication.google.clientID,
  clientSecret: authentication.google.clientSecret,
  callbackURL: authentication.google.callbackURL
}, function(accessToken, refreshToken, profile, done) {
  return done(null, accessToken, profile);
}));﻿

passport.use(new LinkedInStrategy({
  consumerKey: authentication.linkedin.consumerKey,
  consumerSecret: authentication.linkedin.consumerSecret,
  callbackURL: authentication.linkedin.callbackURL
}, function(accessToken, refreshToken, profile, done) {
  return done(null, accessToken, profile);
}));﻿

passport.use(new TwitterStrategy({
  consumerKey: authentication.twitter.consumerKey,
  consumerSecret: authentication.twitter.consumerSecret,
  callbackURL: authentication.twitter.callbackURL
}, function(accessToken, refreshToken, profile, done) {
  return done(null, accessToken, profile);
}));