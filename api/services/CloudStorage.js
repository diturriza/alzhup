'use strict';

/**
 * CloudStorage.js
 *
 * @description A service for manage file on cloud storage providers
 *
 * @author       Edison Galindo <edison@rokk3rlabs.com>
 */

var pkgcloud = require('pkgcloud');

/**
 * Upload a file to rackspace
 * @param req params
 * @param res sails response
 * @param file multipart file
 * @param cb callback
 */

exports.uploadToRackspace = function(filename, next) {

  var remoteContainer = sails.config.appConfig.rackspaceContainer;
  var localContainer = sails.config.appConfig.uploadPath;
  var containerPathCredentials =
    sails.config.appConfig.rackspaceCredentials.containerPath;

  var client = pkgcloud.providers.rackspace.storage.createClient(
    sails.config.appConfig.rackspaceCredentials
  );

  async.waterfall([

    function(cb) {
      client.createContainer(remoteContainer, function(err, container) {
        if (err) {
          cb(err);
          log.error('error ', err);
        } else {
          cb(null, container);
        }
      });
    },
    function(container, cb) {
      client.getContainer(remoteContainer.name, function(err, container) {
        if (err) {
          cb({
            code: 409,
            message: 'Error getting container'
          });
        } else {
          cb(null, container);
        }
      });
    },
    function(container, cb) {
      var rackspacedir = containerPathCredentials + filename;
      var localdir = localContainer + filename;
      log.info('./', localdir, '| >>>>> |', rackspacedir);
      client.upload({
        container: container,
        remote: filename,
        local: localdir
      }, function(err, result) {
        if (err) {
          log.error('error ', err);
          cb(err);
        } else {
          log.info('error / result = ', err, '/', result);
          cb(null, rackspacedir);
        }
      });
    }
  ], function(err, result) {
    log.debug('Rackspace Result: ', result);
    if (err) {
      next(err);
    } else {
      next(null, result);
    }
  });
};