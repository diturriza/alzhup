module.exports = {
 uuid : makeUuid,
 makePassword: makePassword
};

// yea these aren't canonical UUID's they're better
function makeUuid(len) {
  var seed = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var len = len || 32;

  return _.times(len, function(cha){
    return seed[_.random(0, seed.length - 1)];
  }).join('');
};

function makePassword(len){
  return makeUuid(len || 8);
}