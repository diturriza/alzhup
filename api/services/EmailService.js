// This will be moved to AWS
/* jshint camelcase: false */
'use strict';

var mandril_require = require('mandrill-api/mandrill'),
  mandrill = new mandril_require.Mandrill(sails.config.appConfig.mandrillAPI),
  message = {
    to: []
  };

/**
 * set a destinatary to mandril email
 * @param object to: {name:person_name, email:email@person.com}
 * @author Edison Galindo <edison@rokk3rlabs.com>
 */
function setMessageTo(to) {
  message.to.push(to);
}

/**
* Sends an email with the template and vars specified on the params
* @param  Object params should be in this way :
                  {
                    templateName : name of the template in mandrill ,
                    email: the receiver of this message ,
                    templatevars:{
                                  name_of_the_merge_var : value_of_the_mergevar
                                 }
                   }
* @param  {Function} next the callback function
* @author Aryuna <aryuna@rokk3rlabs.com>
*/

function sendEmailTemplateWithVars(params, next) {

  message.to = [{
    "email": params.email
  }];

  var mergevars = propsToMergeVars(params.templatevars);

  message.merge_vars = [{
    "rcpt": params.email,
    "vars": mergevars
  }];

  var templateParams = {
    message: message,
    templateName: params.templateName
  };

  sendMailWithTemplate(templateParams, function(err, data) {
    if (err) {
      next(err);
    } else {
      next(null, data)
    }
  });
}

/**
 * Converts the properties of the specified object to the mergevars array
   needed on the template message
 * @param obj the object from the properties will be extracted
              {name_of_the_merge_var : value_of_the_mergevar }
 * @return an array of the mergevars needed on the mandrill templates
 * @author Aryuna <aryuna@rokk3rlabs.com>
 */
function propsToMergeVars(obj) {
  var array = [];
  _.forOwn(obj, function(val, key) {
    array.push({
      "name": key,
      "content": val
    });
  });
  return array;
}

/**
 * Sends an email with the html and email specified on the params
 * @param  Obj  params {
                         toEmail : the receiver of this message,
                         subject : subject of the email,
                         html : html of the message
                        }
 * @param  {Function} next the callback function
 * @author Aryuna <aryuna@rokk3rlabs.com>
 */
function sendHtmlEmail(params, next) {
  message.subject = params.subject;
  message.from_email = sails.config.appConfig.fromEmail;
  message.from_name = sails.config.appConfig.appName;
  message.html = params.html;
  message.to = [{
    "email": params.toEmail
  }];
  sendMail(next);
}

/**
 * Send a plain email
 * @author Edison Galindo <edison@rokk3rlabs.com>
 */
function sendMail(callback) {
  var async = true,
    ip_pool = '',
    send_at = '';
  var params = {
    "message": message,
    "async": async,
    "ip_pool": ip_pool,
    "send_at": send_at
  };
  mandrill.messages.send(params, function(result) {
    // log('mail send',result);
    if (callback) {
      callback(null, result);
    }
  }, function(error) {
    log.error('A mandrill error occurred: ' + error.name + ' - ' + error.message);
    if (callback) {
      callback(error);
    }
  });
}

/**
 * Send an email using a Mandrill template
 * @author Edison Galindo <edison@rokk3rlabs.com>
 */
function sendMailWithTemplate(params, next) {
  var async = true,
    ip_pool = '',
    send_at = '',
    mandrillObj = {
      "template_name": params.templateName,
      "template_content": [{}],
      "message": params.message,
      "async": async,
      "ip_pool": ip_pool,
      "send_at": send_at
    };
  mandrill.messages.sendTemplate(mandrillObj,
    function(result) {
      // log('mail send', result);
      next(null, result);
    },
    function(error) {
      log.error('A mandrill error occurred: ' + error.name + ' - ' + error.message);
      next(error);
    });
}

module.exports = {
  setMessageTo: setMessageTo,
  sendEmailTemplateWithVars: sendEmailTemplateWithVars,
  propsToMergeVars: propsToMergeVars,
  sendHtmlEmail: sendHtmlEmail,
  sendMail: sendMail
}