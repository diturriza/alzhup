/**
 * Session Configuration
 * (sails.config.session)
 *
 * Sails session integration leans heavily on the great work already done by
 * Express, but also unifies Socket.io with the Connect session store. It uses
 * Connect's cookie parser to normalize configuration differences between Express
 * and Socket.io and hooks into Sails' middleware interpreter to allow you to access
 * and auto-save to `req.session` with Socket.io the same way you would with Express.
 *
 * For more information on configuring the session, check out:
 * http://links.sailsjs.org/docs/config/session
 */

// for parsing below
// example DB_URL
// mongodb://user:pass@kahana.mongohq.com:10043/app27921694
var db = process.env.MONGOHQ_URL;
var confVariables;

if ( db ) {
  var dba = db.split(':');

  confVariables = {
    adapter:'mongo',
    // port: 10043,
    port: dba[3].split('/')[0],
    // app#########
    db: db.substr( db.lastIndexOf('/') + 1 ),
    // username
    username: dba[1].substr(2),
    // password
    password: dba[2].split('@')[0],
    // kahana.mongohq.com
    host: dba[2].split('@')[1],
    collections: 'sessions'
  }
} else {
    // for local dev
  confVariables = {
    adapter: 'mongo',
    host : 'localhost',
    port : 27017,
    db: require('./appConfig.js').appConfig.databaseName,
    collections : 'sessions',
    autoReconnect: true
  };
}

module.exports.session = confVariables;
/*
module.exports.session = {

  // Session secret is automatically generated when your new app is created
  // Replace at your own risk in production-- you will invalidate the cookies of your users,
  // forcing them to log in again.
  secret: '7f8efce04c3b6a123f71223425490882',


  // Set the session cookie expire time
  // The maxAge is set by milliseconds, the example below is for 24 hours
  //
  // cookie: {
  //   maxAge: 24 * 60 * 60 * 1000
  // }


  // In production, uncomment the following lines to set up a shared redis session store
  // that can be shared across multiple Sails.js servers
  // adapter: 'redis',
  //
  // The following values are optional, if no options are set a redis instance running
  // on localhost is expected.
  // Read more about options at: https://github.com/visionmedia/connect-redis
  //
  // host: 'localhost',
  // port: 6379,
  // ttl: <redis session TTL in seconds>,
  // db: 0,
  // pass: <redis auth password>
  // prefix: 'sess:'


  // Uncomment the following lines to use your Mongo adapter as a session store
  // adapter: 'mongo',
  //
  // host: 'localhost',
  // port: 27017,
  // db: 'sails',
  // collection: 'sessions',
  //
  // Optional Values:
  //
  // # Note: url will override other connection settings
  // url: 'mongodb://user:pass@host:port/database/collection',
  //
  // username: '',
  // password: '',
  // auto_reconnect: false,
  // ssl: false,
  // stringify: true

};
*/