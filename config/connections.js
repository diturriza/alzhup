/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.connections.html
 */

// for parsing below
// example DB_URL
// mongodb://user:pass@kahana.mongohq.com:10043/app27921694
var db = process.env.MONGOHQ_URL;
var theMongo;

if ( db ) {
  console.log('Remote Mongo @ ', db);

  theMongo = mongoUrlToParamObj(db);
} else {
  theMongo = {
    adapter: 'sails-mongo',
    host: 'localhost',
    port: 27017,
    user: '',
    password: '',
    database: require('./appConfig.js').appConfig.databaseName,
  };
}

module.exports.connections = {

  /***************************************************************************
  *                                                                          *
  * Local disk storage for DEVELOPMENT ONLY                                  *
  *                                                                          *
  * Installed by default.                                                    *
  *                                                                          *
  ***************************************************************************/
  localDiskDb: {
    adapter: 'sails-disk'
  },


  /***************************************************************************
  *                                                                          *
  * MongoDB is the leading NoSQL database.                                   *
  * http://en.wikipedia.org/wiki/MongoDB                                     *
  *                                                                          *
  * Run: npm install sails-mongo                                             *
  *                                                                          *
  ***************************************************************************/
  mongo: theMongo,



  /***************************************************************************
  *                                                                          *
  * More adapters: https://github.com/balderdashy/sails                      *
  *                                                                          *
  ***************************************************************************/
  mongoUrlToParamObj: mongoUrlToParamObj,
  mongoObjToCommandLine: mongoObjToCommandLine,
  mongoUrlToCommandLineParams: function(url) {
    var obj = mongoUrlToParamObj(url);
    return mongoObjToCommandLine(obj);
  },
};




function mongoUrlToParamObj(url) {
  var dba = url.split(':');

  var theMongo = {
    adapter: 'sails-mongo',
    // port: 10043,
    port: dba[3].split('/')[0],
    // app#########
    database: url.substr(url.lastIndexOf('/') + 1),
    // username
    user: dba[1].substr(2),
    // password
    password: dba[2].split('@')[0],
    // kahana.mongohq.com
    host: dba[2].split('@')[1]
  }
  // console.log(theMongo)
  return theMongo;
}

function mongoObjToCommandLine(obj) {
  var cmdParams = ' --host=' + obj.host + ' --port=' + obj.port;
  cmdParams += ( typeof obj.user !== "undefined" ) ? ' --username=' + obj.user + ' --password=' + obj.password : '';
  cmdParams += ' --db=' + obj.database + ' ';
  // console.log(cmdParams);
  return cmdParams;
}