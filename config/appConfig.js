/**
 * appConfig
 * (sails.config.appConfig)
 *
 * App Config file contains all the constant varibles to be used in the enterir app.
 *
   To use:
   sails.config.appConfig.foo
 * @class appConfig
 * @static
 * @namespace sails.config
 * @module config
 */

module.exports.appConfig = {
  //Use this variable to set a salt for your security encryption.
  appSalt : 'applicationSalt',

  //Use this variable to set your application name and use it on your site.
  appName : 'AlzhUp [' + require('./../package.json').version + ']',

  newRelicKey : {
    qa : '103a23c8b662f815582c24db53017e50ca341925',
    prod: '52673c395dda330a5aa2b52216d749643defbd4b'
  },

  mongoUrl: {
    production: '',
    qa: ''
  },

  databaseName : 'alzhup',

  uploadPath: 'uploads/',

    /** Braintree payments service credentials */
  braintreeAuthObject: {
    development : {
      merchantId: 'asdf1234',
      publicKey: 'asdf1234',
      privateKey: 'asdf1234'
    },
    production : {
      merchantId: 'asdf1234',
      publicKey: 'asdf1234',
      privateKey: 'asdf1234'
    }
  },

  //Madrill credentials, for tests
  mandrillAPI: 'asdf1234',
  fromEmail: 'noreply@r3lproduct.build',
  mandrillPassword: 'asdf1234',

  /**
  * Credentials to Rackspace Cloud Service
  */
  rackspaceCredentials: {
    username: '',
    apiKey: '',
    region: 'IAD',
    containerPath:'.r43.cf5.rackcdn.com/',
  },

  rackspacePostedSongContainer : {
    name: 'Thing',
    metadata: {
      brand: 'Project',
      model: 'Things'
    }
  },

}
