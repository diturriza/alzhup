/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://links.sailsjs.org/docs/config/bootstrap
 */

module.exports.bootstrap = function(cb) {
    _     = require('lodash');
  async   = require('async');
  log     = require('captains-log')();
  request = require('request');
  var braintree = require('braintree');

  // for mock server

  f = {};
  // fs localized to root
  var files = require('fs').readdirSync('./test/fixtures');
  files.forEach(function(file) {
    // require localized to this file
    f[file.slice(0, -3)] = require('./../test/fixtures/' + file);
  });

  //isn't this pretty
  chrono = { start : function(){ this.startDate = Date.now() }, tap: function(){
    if ( !this.hasOwnProperty('startDate') ) { this.start(); }
      this.delta = Date.now() - this.startDate;
      return this.delta;
    }
  };

  /** Find or Create this default user **/
  User.findOrCreate({username: f.user.username}, f.user).exec(function(err, out) {
    log.info('User created by boostrap.js!');
  });

  /* Init braintree paymentGateway */
  if(process.env.NODE_ENV == "production"){
    paymentGateway = braintree.connect({
      environment: braintree.Environment.Production,
      merchantId: sails.config.appConfig.braintreeAuthObject.production.merchantId,
      publicKey: sails.config.appConfig.braintreeAuthObject.production.publicKey,
      privateKey: sails.config.appConfig.braintreeAuthObject.production.privateKey
    });
  } else {
    paymentGateway = braintree.connect({
      environment: braintree.Environment.Sandbox,
      merchantId: sails.config.appConfig.braintreeAuthObject.development.merchantId,
      publicKey: sails.config.appConfig.braintreeAuthObject.development.publicKey,
      privateKey: sails.config.appConfig.braintreeAuthObject.development.privateKey
    });
  }


  // It's very important to trigger this callack method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
