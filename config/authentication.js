
/**
* authentication configuration
*
* To use:
* sails.config.appConfig.foo

* @class authentication
* @static
* @namespace sails.config
* @module config
*/
module.exports.authentication = {
  app: {
    clientKey: 'e37089ab923e91d3907c067a3b83d7500532a065f',
    clientSecret: '8ba4ac8720a6e3b0bed8f837e59c4556'
  },
  /**
  * facebook app setting should set here
  * @property facebook
  */
  facebook: {
    clientID: "482292515227453",
    clientSecret: "ff2c19a93dd6d03eb1f4f26a0b8875dd",
    callbackURL: "http://localhost:1337/oauth/facebook/callback",
    options: { scope: ['email', 'user_about_me'] }
  },
  /**
  * google app setting should set here
  * @property google
  */
  google: {
    clientID: '1091284738784-f62kmem1v8enp3p2evjjsga329eqa3d4.apps.googleusercontent.com',
    clientSecret: 'TGJlgIu_tXJg3tqm8TbjHO77',
    callbackURL: 'http://localhost:1337/oauth/google/callback',
    options: { scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/analytics.readonly'
      ]
    }
  },
  /**
  * linkedin app setting should set here
  * @property linkedin
  */
  linkedin: {
    consumerKey: '773e8rddvzb6tx',
    consumerSecret: 'lAjdJsPqPcG3wYM6',
    callbackURL: 'http://localhost:1337/oauth/linkedin/callback',
    options: {}
  },
  /**
  * twitter app setting should set here
  * @property twitter
  */
  twitter: {
    consumerKey: 'ogtpFfYYLw0NQPWUWRReIojFZ',
    consumerSecret: 'u1T2hYHlpd9atFHU2a2ZTYue6C4gRljnmEB8sXetxzxAIgkaKF',
    callbackURL: 'http://localhost:1337/oauth/twitter/callback',
    options: {}
  }
};
