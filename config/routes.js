/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `config/404.js` to adjΩt your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on routes, check out:
 * http://links.sailsjs.org/docs/config/routes
 */

module.exports.routes = {

  // Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, etc. depending on your
  // default view engine) your home page.
  //
  // (Alternatively, remove this and add an `index.html` file in your `assets` directory)
  'GET /login': 'auth.login',
  'POST /login': 'auth.process',
  'POST /logout': 'auth.logout',
  'POST /login/client': 'auth.processClient',
  // Custom routes here...

  '/oauth/:serviceName': {
    controller: 'oauthExternal',
    action: 'externalService'
  },
  '/oauth/:serviceName/callback': {
    controller: 'oauthExternal',
    action: 'externalCallback'
  },
  'POST /oauth/access_token': 'auth.processClientCallback',

  '/': {
    controller: 'home',
    action: 'index'
  },

  'POST /user': 'UserController.createUser',
  'POST /user/caregiver': 'UserController.addCaregiver',
  'POST /user/provider': 'UserController.addProvider',

  'PUT /user/:id': 'UserController.updateUser',

  'GET /user/contacts': 'UserController.getContacts',
  'GET /user/calendar': 'UserController.getCalendar',
  'GET /user/messages': 'UserController.getMessages',
  'GET /user/:id': 'UserController.getUser',

  'DELETE /user/caregiver/:id': 'UserController.removeCaregiver',
  'DELETE /user/provider/:id': 'UserController.removeProvider',

  'POST /activity': 'ActivityController.createActivity',
  'PUT /activity/:id': 'ActivityController.updateActivity',
  'GET /activity/next': 'ActivityController.getNextActivity',
  'GET /activity/list': 'ActivityController.getActivityList',
  'GET /activity/:id': 'ActivityController.getActivity',
  'DELETE /activity/:id': 'ActivityController.destroyActivity',

  'POST /system/competancy' : 'TagController.addCompetancy',
  'POST /system/bank' : 'TagController.addMemoryBank',
  'GET /system/competancy' : 'TagController.getCompetancyList',
  'GET /system/bank' : 'TagController.getMemoryBankList',

  'GET /tag/custom' : 'TagController.getCustomTags',
  'GET /tag/people' : 'TagController.getPeopleTags',
  'POST /tag/custom' : 'TagController.addCustomTag',
  'POST /tag/people' : 'TagController.addPeopleTag',
  'DELETE /tag/custom/:id' : 'TagController.disableCustomTag',
  'DELETE /tag/people/:id' : 'TagController.disablePeopleTag',

  'POST /memory' : 'MemoryController.addMemory',
  'GET /memory/featured' : 'MemoryController.getFeaturedMemory',
  'GET /memory/:id' : 'MemoryController.getMemory',
  'GET /memory/find/bank/:bankId' : 'MemoryController.getMemoriesByBank',
  'GET /memory/find/:tagType/:tagId' : 'MemoryController.getMemoriesByTag',
  'GET /memory/list/:startDate/:limit/:skip' : 'MemoryController.listMemoriesByDate',
  'POST /memory/:id/upload' : 'MemoryController.uploadMemory',
  'PUT /memory/:id/details' : 'MemoryController.updateMemoryDetails',
  'PUT /memory/:id/date' : 'MemoryController.updateMemoryDate',
  'PUT /memory/:id/tag/:tagId' : 'MemoryController.linkTag',
  'PUT /memory/:id/face/:personTagId/:topLeft/:bottomRight' : 'MemoryController.linkface',
  'DELETE /memory/:id/tag/:tagId' : 'MemoryController.unlinkTag',
  'DELETE /memory/:id/face/:personTagId' : 'MemoryController.unlinkFace',
  'DELETE /memory/:id' : 'MemoryController.deleteMemory',

  'POST /event' : 'EventController.addEvent',
  'PUT /event/:id' : 'EventController.updateEvent',
  'GET /event/list/:startDate/:numDays' : 'EventController.getEventList',
  'GET /event/:id' : 'EventController.getEvent',
  'DELETE /event/:id' : 'EventController.removeEvent',

  'GET /activity/session/start' : 'ActivitySessionController.startActivitySession',
  'GET /activity/session/end' : 'ActivitySessionController.endActivitySession',
  'POST /activity/session/progress' : 'ActivitySessionController.progressActivitySession'



  // If a request to a URL doesn't match any of the custom routes above,
  // it is matched against Sails route blueprints.  See `config/blueprints.js`
  // for configuration options and examples.

};