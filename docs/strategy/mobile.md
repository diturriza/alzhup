/*
Title: Mobile & Tablet
Sort: 1
*/

# Mobile Strategy

Support Mobile / Tablet Web Browsers for Caregivers and Public

# Tablet Strategy

Patients are native tablet app only, store memories locally to enable offline access.