/*
Title: Performance
Sort: 1
*/

# Performance Strategy
If we use a facial recognition service, it will need a worker pipeline.

## Performance Sensitive Events

Start Activity
Investigate pre-loading.

Access Memories
Cache on device

Search Memories
Index by user. Shard DB as needed.

Display Face Swarm
Cache data. Automate testing for 100+ faces