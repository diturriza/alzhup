/*
Title: SEO
Sort: 1
*/

# SEO Strategy


What are the events we wish to track?

# Google Analytics

What is the GA ID?
What user events are we tracking?
What user segments are we tracking?

[How to pass page titles for SPA](https://developers.google.com/analytics/devguides/collection/analyticsjs/single-page-applications)

# VWO

How are we implementing VWO?
Implement Adept Scale for Automation?
What is the SEO Strategy for this application?

What are the conversion goals?

