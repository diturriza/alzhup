/*
Title: Main
Sort: 1
*/

# Main Interface

What is the primary interface for this application?

What are the actions a user can take in this interface?

What information is needed to drive this interface?

What information do we need to capture from this interface?