/*
Title: User Events
Sort: 1
*/

# Interface Events

What events should the interface emit?
What client-side actions should happen with each event?
What server-side actions should happen with each event?
What events are we tracking? [see](strategy/seo.md)


emailSent
emailError
emailSuccess

When a new user is made, send them an email.