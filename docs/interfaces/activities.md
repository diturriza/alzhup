/*
Title: Activities
Sort: 1
*/

# Area
## Motivation
## Psychological Activity
## Attention
## Memory
## Thought / Language
## Relaxation / Autobiographic Memory

# Daily
Users should be presented with one activity from each area, **in order**.

# Timeouts
1. Track 4 minutes without input. 
2. Display Friendly Message ( OK? -> Restart Timer )
3. After 3 more minutes, end session, record results, but mark as incomplete.
4. Start same session again same day, or later

# Difficulty Factors
Change Time Limit
Button Size
Memory Size
Text Size
Movement / Touch Areas
Number of Visual Objects

