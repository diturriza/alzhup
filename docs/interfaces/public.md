/*
Title: Public / Marketing
Sort: 1
*/

# Public Pages

What is on the marketing site?
What is the copy for the marketing site?
Where are the images sourced for the marketing site?
Is there a video for the marketing site?
Is there a layout/design for the marketing site?

What are we collecting from the marketing site?
How does the site register new users?


