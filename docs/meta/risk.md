/*
Title: Risks & Opporunities
Sort: 1
*/

## HIPAA Compliance

see http://awsmedia.s3.amazonaws.com/AWS_HIPAA_Whitepaper_Final.pdf


What is the primary opportunity of this project?
What could stop this project from succeeding?
How can we prepare for or avert this risk area?

What other doors open when this project is done?
What other risks do we face when the project is done?

| Risk Area     | Solution                  | Who Responsible   |
|---------------|---------------------------|-------------------|
|               |                           |                   |
