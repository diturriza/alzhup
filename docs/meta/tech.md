/*
Title: Technology & Requirements
Sort: 1
*/

## Technology

Node.js - Server side language.

Sails - Server side framework.

Mongo - Database

Angular - Front end framework

## Requirements
Screen Size?
Operating Systems?
Browsers?
Mobile / Tablet?