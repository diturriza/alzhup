/*
Title: Users & Profiles
Sort: 1
*/

## Admin
Web Only

Admins can do customer service tasks. 
-- v+
Admins can [analyze data](../code/algos.md)

## Caregivers
Web ( alongside patient with iPad )
-- v+
iPhone

Caregivers can:
- bulk add memories
- edit memories
- see patient progress
- add/edit contacts
- send notifications


## Patients
iPad - Offline Enabled

Patients can:
- add memories
- edit memories
- view memories
- do activities
- get reminders
- access caregiver infomation

## Public

Public users can only see the marketing site.