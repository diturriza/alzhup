/*
Title: Feature List
Sort: 1
*/


# core feature set

## iPad App

### Login
### Session

## Web App
### Signup / Login
### Manage Memories

# v1

## iPad App
For Patients and Patient & Caregiver to use together

## In App Purchase
Needs to enable monthly subscriptions.

## TouchID Authentication
Vs Login

## Share Extension
Allows adding photo as memory from any iOS application that enables share menu.

## Calendar Sync
AlzhUp patient events are added to local calendar

## Contact Sync
Patients can add Caregivers from Contacts

## Device Integrations
Photo Roll, iTunes Library, Microphone, Camera, Contacts, Notifications, Calendar, GPS

### Internationalization
English and Spanish to Start

### Data Sync
Application should be 100% functional without an internet 

### Background / Push Notifications
Update messages, events and new memories addition from web app

### Models
Activities - Interactions (mini-games) which are loaded dynamically and return data
Memories - Audio, Photo or Videos which are tagged and retrieved
MemoryBank / Competancy - Assigned to Memories
Tags - People or Custom
Face - People have Face rectangles associated with Memory tags
Session - A collection of activities, determined by server
Contact - User Information, Patient <-> Caregiver = many to many
Event - Sent from Server, shows up in app and device calendar
Message - One way communication from Caregiver to Patient

## Login
## Signup
### Sign up new users
- Collect Name, Password, Email, Phone and Address (geocode)
- Input Date of Diagnosis and Date of Birth
- Add / Upload Photo

### Sign up caregivers
- Collect Name, Email, Phone and Address (geocode)
- Add / Upload Photo

## Session
- Begin Session
- Session Overview Page
- Start Activity
- Finish Activity
- Start next activity
- Schedule Activity ( background, no interface )
- Finish Session
- Session Timeout ( display message after 4 minutes, stop session after 5)

## Activity (15x)
- Tutorial
- Populate with memories
- Specialized Interactions
- Measure Performance and Update Competancies

## Memories
- Access
    + Stored on device where possible
- Import
    + Device Capture
    + Upload
    + By Url
    + Platforms
        * Facebook
        * YouTube
- Photo
    + Notes
    + Details
    + Date
    + Tags
    + Person
    + Face

- Video
    + Notes
    + Details
    + Date
    + Tags
    + Person

- Memory Banks
    + Events
    + Life Phase
    + Identity
    + Current
    + Traditions
    + People
    + Things
    + Places
    + Actions

- Memory Interfaces
    + Timeline
        * Horizontal Slider Shows Date / Age
    + Search
        * Tag
        * Memory Bank
        * Person
    + Memory Details
        * Inline Editing
    + Family Tree
        + Person Details Page
            * List of Memories by Person
    
- Home Screen
    + Logout
    + Switch Caregiver
    + Weather
    + Recent Message
    + Messages
        * Read Past Messages
        * Platform Send   
        * Receiver Manage
        * Caregiver Send
        * Read Latest Message
    + Activities
        * Daily Progress
        * Upcoming Session / Completed Session
        * Start Session
    + Calendar
        * Show Month / 3 Days / Daily
        * Show all Day events
        * Next / Prev Day / Month
    + Caregivers
        * Medical
        * Personal
        * Location
        * Contact
        * Schedule
    + Settings / Profile
        - Add Calendar Events / Recurring
        - Manage Caregivers
        - Limit Device Memory Usage (2gb to start)

## Admin
- Users
    + CRUD
- Activity
    + CRUD
    + Assign Competancies
    + Assign Banks
    + Add Tutorial
        * Series of Images


## Web App
For Caregivers
Internationalization (English & Spanish)

### Twilio Integration
SMS to Patient / Caregivers
Sends code to login to app
Invite Caregivers

- Register
- Login
- Pay
- Home 
    - Send Messages
    - Set Reminder
    - See Progress
    - Update Caregiver Information
    - Add all-day events
    - See Patient Schedule
    - Journal
- Manage memories
    + List
    + Remove
    + Import Photos / Video / Audio
    + Add By Url
    + Import Via Social Media
    + Editing 
        * Memory Banks
        * People
        * Faces
        * Custom Tags
    + Remove Tags
    + Add Date
    + Edit Details
- Manage People
    + Add
    + Update
    + Select Face
    + Remove
    + List

# v2 (next release)

## Admin
Memory Explorer

## Caregiver
- Watch Activities
    - Record Observartions
    - See Performance
    - Record Score
        Participation 1-10
        Emotivity 1-10



## iPhone App
For Caregivers

# v+ (later release)

# v? (maybe)

## Patients



## Caregiver

Hire someone to upload memories
Community Forums in Caregiver App
