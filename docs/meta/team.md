/*
Title: AlzhUp Team
Sort: 1
*/

## Product Leads
UX - Joel Mena joel@rokk3rlabs.com
Tech - Sean Canton sean@rokk3rlabs.com

## Biz Dev
Eric Kuhn eric@rokk3rlabs.com
Megan Conyers megan@rokk3rlabs.com

## Advisors
Lorenzo deLeo lorenzo@rokk3rlabs.com
Charles Irizarry charles@rokk3rlabs.com
Rafael Espinosa de los Montereos respinosa@alzhup.com



