FORMAT: 1A
HOST: http://localhost:1337

# Group Memory

## Memory Endpoint [/memory]

### addMemory [POST]

+ Request
    
    {
        resourceUrl: 'http://placekitten.org/200/200',
        resourceType: 'photo',
        details: 'A photo of a cat',
        date: ''
    }

+ Response 200 (application/json)
    
        {
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getMemory [GET]

+ Parameters
    * id (string) - An unique identifier of a memory

+ Response 200 (application/json)

        {
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getMemoriesByTag [GET /find]

+ Parameters
    * tagType (string) - A type of Tag (Custom/Person)
    * tagId (string) - An unique identifier of a tag

+ Response 200 (application/json)

        [{
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

### getMemoriesByBank [GET /find/bank]

+ Parameters
    * bankId (string) - An unique identifier of a memory Bank

+ Response 200 (application/json)

        [{
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

### uploadMemory [POST /upload]

+ Request
    
    File

+ Response 200 (application/json)

        {
            message: 'File has uploaded to server'
        }

### updateMemoryDetails [PUT /:id/details]

+ Parameters
        + id (string) - An unique identifier of the memory

+ Request 

        {
            text: 'text'
        }

+ Response 200 (application/json)

        {
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### updateMemoryDate [PUT /:id/date]

+ Parameters
        + id (string) - An unique identifier of the memory

+ Request 

        {
            date: 'someDate'
        }

+ Response 200 (application/json)

        {
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### linkTag [PUT /:id/tag/]

+ Parameters
        + id (string) - An unique identifier of the memory
        + tagId (string) - An unique identifier of the tag


+ Response 200 (application/json)

        {
            type: 'custom',
            name: 'Custom Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1'
        }

### unlinkTag [DELETE /:id/tag/]

+ Parameters
        + id (string) - An unique identifier of the memory
        + tagId (string) - An unique identifier of the tag


+ Response 200 (application/json)

        {
            type: 'custom',
            name: 'Custom Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1'
        }

### linkFace [PUT /:id/face/]

+ Parameters
        + id (string) - An unique identifier of the memory
        + personTagId (string) - An unique identifier of the face
        + topLeft (array) - Top Left
        + bottomRight (array) - Bottom Right


+ Response 200 (application/json)

        {
            personTagId: '1',
            topLeft: [],
            bottomRight: []
        }

### unlinkFace [DELETE /:id/face/]

+ Parameters
        + id (string) - An unique identifier of the memory
        + personTagId (string) - An unique identifier of the face


+ Response 200 (application/json)

        {
            personTagId: '1',
            topLeft: [],
            bottomRight: []
        }

### deleteMemory [DELETE]

+ Parameters
        + id (string) - An unique identifier of the memory


+ Response 200 (application/json)

        {
            message: "Memory has been removed"
        }

### getFeaturedMemory [GET /featured]

+ Response 200 (application/json)

        {
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getMemoryUpdates [GET /featured]

+ Response 200 (application/json)

        [{
            resourceUrl: 'http://placekitten.org/200/200',
            resourceType: 'photo',
            details: 'A photo of a cat',
            date: 'someDate',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }]