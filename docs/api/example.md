FORMAT: 1A
HOST: http://localhost:1337

see https://github.com/apiaryio/api-blueprint/tree/master/examples

# Group Test

## Test Endpoint [/test]
Do the things

## Test Get [GET]

+ Response 200 (text/plain)

        Hello

## Test Post [POST]

+ Request

        {
          "test": 1234
        }

+ Response 200 (application/json)

        {
          "hello": "world"
        }
