FORMAT: 1A
HOST: http://localhost:1337

# Group Competancy & Memory Bank

## Competancy Endpoint [/system/competancy]
Do competancies

## getCompetancyList [GET]

+ Response 200 (application/json)

        [{
        "id": "abc123",
        "name": "Tactile Perception"
        }]

## addCompetancy [POST]

+ Request (text/plain)

        Tactile Perception

+ Response 200 (application/json)

        {
        "id": "abc123",
        "name": "Tactile Perception"
        }

