FORMAT: 1A
HOST: http://localhost:1337

# Group Activity Session

## Activity Session Endpoint [/activity/session]

### startActivitySession [GET]

+ Response 200 (application/json)
        
        {
            userId: '1',
            startTime: '',
            EndTime: '',
            complete: false,
            results: [],
            competancyAggregate: []
        }

### endActivitySession [GET]

+ Response 200 (application/json)
        
        {
            userId: '1',
            startTime: '',
            EndTime: '',
            complete: false,
            results: [],
            competancyAggregate: []
        }

### ProgressActivitySession [POST]

+ Request
        
        {
            userId: '1',
            startTime: '',
            EndTime: '',
            complete: false,
            results: [],
            competancyAggregate: []
        }

+ Response 200 (application/json)  
 
        {
            message:"Progress Activity Session"
        }