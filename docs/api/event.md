FORMAT: 1A
HOST: http://localhost:1337

# Group Event

## Event Endpoint [/event]

### addEvent [POST]

+ Request

        {
            contactId: '1',
            userId: '1',
            eventTime: new Date(),
            eventType: null,
            title: 'title Test',
            details: 'Details Test'
        }

+ Response 200 (application/json)

        {
            contactId: '1',
            userId: '1',
            eventTime: new Date(),
            eventType: null,
            title: 'title Test',
            details: 'Details Test',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### updateEvent [PUT]

+ Parameters
    * id (string) - An unique identifier of an Event

+ Response 200 (application/json)

        {
            contactId: '1',
            userId: '1',
            eventTime: new Date(),
            eventType: null,
            title: 'title Test',
            details: 'Details Test',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getEvent [GET]

+ Parameters
    * id (string) - An unique identifier of an Event

+ Response 200 (application/json)

        {
            contactId: '1',
            userId: '1',
            eventTime: new Date(),
            eventType: null,
            title: 'title Test',
            details: 'Details Test',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getEventList [GET /list]

+ Parameters
    * startDate (date) - Start date of the list
    * numDays (integer) - Number of days covered by the list

+ Response 200 (application/json)

        [{
            contactId: '1',
            userId: '1',
            eventTime: new Date(),
            eventType: null,
            title: 'title Test',
            details: 'Details Test',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

### removeEvent [DELETE]

+ Parameters
    * id (string) - An unique identifier of an Event

+ Response 200 (application/json)  
 
        {
          message: "Activity has been removed"
        }


