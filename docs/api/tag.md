FORMAT: 1A
HOST: http://localhost:1337

# Group Tag

## Tag Custom Endpoint [/tag/custom]

### getCustomTags [GET]

+ Response 200 (application/json)

        [{
            type: 'custom',
            name: 'Custom Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

### addCustomTag [POST]

+ Request

        {
            type: 'custom',
            name: 'Custom Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1'
        }

+ Response 200 (application/json)

        {
            type: 'custom',
            name: 'Custom Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### disabledCustomTag [DELETE]

+ Parameters
    * id (string) - An unique identifier of a Custom Tag

+ Response 200 (application/json)

        {
            "message": "Tag has been removed"
        }

## Tag People Endpoint [/tag/people]

### getPeopleTags [GET]

+ Response 200 (application/json)

        [{
            type: 'person',
            name: 'people Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

### addPeopleTag [POST]

+ Request

        {
            type: 'person',
            name: 'people Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1'
        }

+ Response 200 (application/json)

        {
            type: 'person',
            name: 'people Tag',
            contactId: '1',
            relationship: 'relationshipTest',
            active: true,
            userID: '1',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### disabledPeopleTag [DELETE]

+ Parameters
    * id (string) - An unique identifier of a people Tag

+ Response 200 (application/json)

        {
            "message": "Tag has been removed"
        }

