FORMAT: 1A
HOST: http://localhost:1337

# Group Activity

## Activity Endpoint [/activity]

### createActivity [POST]

+ Request

        {
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization'
        }

+ Response 200 (application/json)

        {
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### updateActivity [PUT]

+ Parameters
    * id (string) - An unique identifier of an Activity

+ Request

        {
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization'
        }

+ Response 200 (application/json)

        {
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        } 

### getActivity [GET]

+ Parameters
    * id (string) - An unique identifier of an Activity

+ Response 200 (application/json)

        {
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getNextActivity [GET /next]

+ Response 200 (application/json)

        {
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getActivityList [GET /list]

+ Response 200 (application/json)

        [{
            gameId: 1,
            memoryBanks: [],
            competancies: [],
            area: 'area test',
            function: ' function test',
            title: 'title test',
            description: 'description test',
            recommendedTime: 5,
            difficulty: 5,
            externals: 'externals test',
            variations: 'variations test',
            personalization: 'personalization',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...] 

### destroyActivity [DELETE]

+ Parameters
    * id (string) - An unique identifier of an Activity

+ Response 200 (application/json)  
 
        {
          message: "Activity has been removed"
        }