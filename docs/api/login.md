/*
Title: Emails
Sort: 1
*/

FORMAT: 1A
HOST: http://localhost:1337

# POST /login

+ Request

        {"username":"admin","password":"password"}

+ Response 200 (application/json; charset=utf-8)

    + Body

            {
              "status": "ok",
              "results": {
                "firstname": "Admin",
                "lastname": "User",
                "username": "admin",
                "isAdmin": true,
                "createdAt": "2015-05-18T18:35:42.554Z",
                "updatedAt": "2015-05-18T18:35:42.554Z",
                "id": "555a30fee3827da920aa9196"
              }
            }