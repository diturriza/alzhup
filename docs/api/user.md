FORMAT: 1A
HOST: http://localhost:1337

# Group User

## User Endpoint [/user]
Do Users

### addUser [POST]

+ Request 

        {
            firstname: 'William' ,
            lastname: 'Powell,
            username: 'william.powell',
            password: '1234',
            isAdmin: true
        }

+ Response 200 (application/json)

        {
            firstname: 'William' ,
            lastname: 'Powell,
            username: 'william.powell',
            password: '1234',
            isAdmin: true,
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }


### updateUser [PUT]

+ Parameters
        + id (string) - An unique identifier of the user

+ Request 

        {
            firstname: 'William' ,
            lastname: 'Powell,
            username: 'william.powell',
            password: '1234',
            isAdmin: true
        }

+ Response 200 (application/json)

        {
            firstname: 'William' ,
            lastname: 'Powell,
            username: 'william.powell',
            password: '1234',
            isAdmin: true,
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

### getUser [GET]

+ Parameters
    + id (string) - An unique identifier of the user

+ Response 200 (application/json)

        {
            firstname: 'William' ,
            lastname: 'Powell,
            username: 'william.powell',
            password: '1234',
            isAdmin: true,
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        }

## User Caregiver Endpoint [/user/caregiver]
Do Caregiver Contacts

### addCaregiver [POST]

+ Request

        {
            name: 'Alan',
            phone: '999 999-9999',
            address: 'Caracas',
            location: ['10.4653533', '-66.8881343,12'],
            relationship: 'Test relationship',
            isUser: false,
            details: 'Details Test',
            photoURL: 'http://ww1.prweb.com/prfiles/2013/10/19/11249836/1231435_625100317511896_1454519309_n.jpg',
            userId: ''
          }

+ Response 200 (application/json)

        {
            name: 'Alan',
            phone: '999 999-9999',
            address: 'Caracas',
            location: ['10.4653533', '-66.8881343,12'],
            relationship: 'Test relationship',
            isUser: false,
            details: 'Details Test',
            photoURL: 'http://ww1.prweb.com/prfiles/2013/10/19/11249836/1231435_625100317511896_1454519309_n.jpg',
            userId: '',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
          }

### removeCaregiver [DELETE]

+ Parameters
    + id (string) - An unique identifier of the caregiver

## User Provider Endpoint [/user/provider]
Do Provider Contacts

### addProvider [POST]

+ Request

        {
            name: 'Alan',
            phone: '999 999-9999',
            address: 'Caracas',
            location: ['10.4653533', '-66.8881343,12'],
            relationship: 'Test relationship',
            isUser: false,
            details: 'Details Test',
            photoURL: 'http://ww1.prweb.com/prfiles/2013/10/19/11249836/1231435_625100317511896_1454519309_n.jpg',
            userId: ''
          }

+ Response 200 (application/json)

        {
            name: 'Alan',
            phone: '999 999-9999',
            address: 'Caracas',
            location: ['10.4653533', '-66.8881343,12'],
            relationship: 'Test relationship',
            isUser: false,
            details: 'Details Test',
            photoURL: 'http://ww1.prweb.com/prfiles/2013/10/19/11249836/1231435_625100317511896_1454519309_n.jpg',
            userId: '',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
          }

### removeProvider [DELETE]

+ Parameters
    + id (string) - An unique identifier of the provider

## User Contacts Endpoint [/user/contacts]

### getContacts [GET]

+ Response 200 (application/json)

        [{
            name: 'Alan',
            phone: '999 999-9999',
            address: 'Caracas',
            location: ['10.4653533', '-66.8881343,12'],
            relationship: 'Test relationship',
            isUser: false,
            details: 'Details Test',
            photoURL: 'http://ww1.prweb.com/prfiles/2013/10/19/11249836/1231435_625100317511896_1454519309_n.jpg',
            userId: '',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

## User Events Endpoint [/user/calendar]

### getCalendar [GET]

+ Response 200 (application/json)

        [{
            contactId: '1',
            userId: '1',
            eventTime: new Date(),
            eventType: null,
            title: 'title Test',
            details: 'Details Test',
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]

## User Messages Endpoint [/user/messages]

### getMessages [GET]

+ Response 200 (application/json)

        [{
            senderContactId: '1',
            userId: '1',
            message: 'Test Message',
            title: 'Test Tile',
            unread: false,
            id : 'idFromMongo',
            createdAt: 'someDate',
            updatedAt: 'someDate'
        },...]
