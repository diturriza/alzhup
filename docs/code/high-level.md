/*
Title: High Level
Sort: 1
*/


## Activity Session
![session2](../images/session_flow.png)
![session](../images/session.png)

## My Memories
A collection of media (photo, video, audio), metatagged with System level tags (MemoryBanks), and User level tags (people, custom)

## Calendar
Caregivers can schedule things for Patients. Calendar is kept on server and synced with device via background service.

## Messages
Caregivers can write messages to Patients.

## Contacts

## Testing
Test as much as reasonable.

What are the larger architectural pieces?

Feel free to include diagrams.

Is your code going to be regression tested to prevent old bugs from resurfacing?

