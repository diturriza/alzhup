/*
Title: Scopes
Sort: 1
*/

Clear divisions between responsibilities.

## iOS Core
High Level: Login. Display and execute daily sessions, consisting of 6 activities. Activities are simple interactives which need to be modular in their inputs, meaning, images and words used in activities are dynamic (determined by server). 


## iOS Architecture

High Level:  Connect to endpoints, enable device integrations, build out models appropriately, figure out login and syncs, share extension, and build up controllers. Modularity and flexibility is key.

## iOS Views

High Level: Implement designs and interactions using Material Design approach. Things should feel solid. 

## iOS Activities

High Level: Activities are a series of integrated mini-games. Develop a module to input an integer and some configuration data and load an Activity. Activities will return aggregations of event data and be managed in a queue called a Session. Make individual Activities based off ICAP specifications and provided documentation.

## iOS Interactions (Tree/Timeline)

High Level: Memories are photos with meta information about date, context and people. Timeline is a linear scrolling window through time. Tree is a dynamic representation of people and relationships, similiar to a graph diagram. Each needs to be high-performance and driven by complex sets of data. 

## BE Dev

High Level: Node & Mongo. Support the buildout for iOS, Admin and Caregiver Web App. Implement algorithms for Activities, Sessions and Memories.

## FE Dev

High Level: Angular, HTML, CSS. Make Cr

## Designer

## UX Researcher (agency)

## Information Architect
