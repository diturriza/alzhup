/*
Title: Algorithms
Sort: 1
*/

# Admin
## Track Per Activity Effectiveness
Activities return competancies. Aggregate competancy improvement over time.

## Track Correlations Across Demographics


# Patient
## Validated / Confirmed Memories

# [Activities](../interfaces/activities.md)

## Build Daily list of Activities (v1)
### Activity Selection
Random at first. As patient competancies are discovered, select for strong and weak areas. 
### Activity Order 
Determined by [activity category].

### Memory Determination
Find Memories Via MemoryBanks from Activity MemoryBanks
Prioritize memories by meta.weight
Reinforce memories selected in Session
( Advanced )
Add. prioritize people / custom / faces tags

### With different difficulty levels (v+)
Use patient diagnosis level to determine difficulty.

## Scoring Activity with Competancies (V1)
## Each Accomodates Rising Levels of Difficulty

# Memories

How do we avoid burying memories with low scores?

## Memory Weighting
Access Memory +1
Add Person Tag +3
Add Custom Tag +1
Add Face +5
Featured +10

## Determine Featured Memory
Lowest meta.weight memory (?).

## Using Memories in an Activity increases Relevance Score
Tends to self-select popular memories. Maybe if competancy is low?
## Accessing People adds Relevance Score
Tree Access of a Person = +1 Weight to all Memories with that person
## Storing memories locally vs on the server
Use settings.storageLimit and remove lowest.weight memories

