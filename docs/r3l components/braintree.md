/*
Title: Braintree - Payments
Sort: 1
*/

## Braintree Service

### customerCreate

Create a customer first

### subscription

Subscribe a customer

### subscriptionDetails

Retrieve details for a user's subscription

### transactionRefund

Refund a transaction