/*
Title: XAuth / OAuth
Sort: 1
*/

## Auth Docs

### Xauth:

Uncomment config/oauthProvider.js:120 to test without adding a user.

#### Serverside:

Handled in AuthController.processClient

POST `{ username, password, key }` json to `/login/client` endpoint.

Server makes internal POST to `oauth/access_token`

Passes off to AuthController.processClientCallback. This function needs to be there to enable middleware.

If user is authenticated, original POST will respond with
`{  expirationDate, username, accessToken }`

To the Client

#### Clientside:

`assets/js/services/auth.js` has `xauth` to make the server call. The username and password are hard coded here, as a demonstration of what the server is expecting.
`logincontroller.js` handles the `doXauth` method and points it to the Auth Service.
