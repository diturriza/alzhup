/*
Title: Data Generation
Sort: 1
*/

## Dummy data

What dummy data do you need to make?

It's important to be able to generate, programatically, all aspects of the application, so we can start from a blank slate and get to a testable scenario very quickly. 

This is also useful for generating endpoints for iOS or client side testing before the actual objects are ready. 

[Faker](https://github.com/Marak/Faker.js) is useful for this endeavor, note it is UK centric.



