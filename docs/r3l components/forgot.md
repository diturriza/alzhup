/*
Title: Forgot Password
Sort: 1
*/

## Forgot Password
![Forgot Password](%image_url%/forgotFlow.png)
Send email which links to site with token to change password.