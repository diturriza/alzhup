/*
Title: Databases
Sort: 1
*/

## Databases

( models and associations are stored in code )

What database(s) are you using?
What records are stored in each database?
What is the distributed / scaling strategy?
