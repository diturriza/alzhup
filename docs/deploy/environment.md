/*
Title: Environment
Sort: 1
*/

## IP
```
dev - 10.0.0.1
qa - xx.xx.xx.xx
dev - xx.xx.xx.xx
stage - xx.xx.xx.xx
production - xx.xx.xx.xx
```

## Server Environment 

How are the servers arranged?

## Staging Environment

What is the staging environment?

## Client Environments

## Code Management / Deployment
What is your strategy for pushing code from local machines, to QA and to Production.

### Web
What screen sizes / browsers does the application support?

### Mobile / Tablet
Native, wrapper or compiled?
What device sizes does the application support?