/*
Title: Dev ReadMe
Sort: 1
*/

## Hello Developer

We are so happy to have you working with us on this project. This node-stack is a labor of love on the part of all of us, and your contributions are a big part of that. Whether in documentation or code, what you contribute to your projects can be made part of the group mind. We all get a little better for having you pass through. 

## Wiki
Please visit the [wiki](http://weco.build) for our developer best practices.

## Driven To Excellence
r3l values data, documentation & tests ensure our code is up to standards, and easier for whoever has to main 

## Gulp
We use gulp for many many things. Here is a list of gulp commands.

````
    gulp watch  - Builds, lifts, watches for changes, rebuilds
    gulp watch:debug - Builds, runs nodemon & node inspector
    gulp watch:prod - Watch a production Server
    gulp server - Builds, lifts, opens browser
    gulp server:prod - Serve in Production mode
    gulp test - Runs all tests, might have to run 2x
    gulp build - Build all front end code
    gulp debug - Log all input file paths
    gulp check - Run jshint on all the things
    gulp todo - Collects TODO, OPTIMIZE, NOTE, HACK, XXX, FIXME, BUG comments
    gulp db:prod:local - Copies production database to local
````

## 


## Questions, Problems, Concerns or Anolomies

Problems with the node-stack, ideas for what we can do? 

Contact sean or brian

