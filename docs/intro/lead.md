/*
Title: Project Lead
Sort: 1
*/

![Rokk3r Labs](/images/logo.png)


## So you are leading a project...

First, congratulations. We're doing our best here to provide you with a cutting-edge, versatile software stack to develop awesome r3l products. 

Second, leading is not easy. This documentation is an ongoing effort to provide best practices and collect knowledge from products we build. 

Third, help out other leads. Please collect lessons learned as adding questions to ask in node-stack.

# Documentation
The `/docs` folder serves as a checklist for all the high-level project considerations. Please link to external bookmarks in readme.md. [#](#bookmarks)

Document in code as much detail as possible. This makes it easier for build to execute. Models, endpoints and payloads should be documented in code after being outlined in the API spec.


# Checklists

## Partner Needs to Buy / Signup
BrainTree Account - Sandbox & Production

Mandrill Account - Email

Hosting Account - Heroku / Digital Ocean / Rackspace

## Administrative

Make users on server for devs

Add appropriate devs to /etc/sudoers

Update `README.md` with links to designs, wireframes, tech specs, etc. 

Clone node-stack repo

Name project in package.json / appConfig.js

Change database name in connections.js and session.js

Change out test databases in test/unit.test.js and test/api.test.js

( Optional ) If using sails templates, uncomment '/' : object in routes.js 

Push to new Bit Bucket Repo

Switch default branch to dev

Add google analytics tracking code.

Add Models as Globals to .jshintrc

Make New Slack Team

## Onboarding

Invite members to Bitbucket / Pivotal / Crashlytics / Slack

## Deployment
Heroku (deploy/heroku.md)

make Heroku QA and Production App
add { NODE_ENV : production } to production>settings>config vars

### Database
add Compose to qa/prod - `heroku addons:add mongohq`

From heroku>settings>config vars
update appConfig.mongoUrl with MONGOHQ_URL for prod and qa
update appConfig.databaseName 

Add indexes

### Monitoring
setup google analytics account, add google analytics code

add New Relic to each - `heroku addons:add newrelic:stark`

From heroku>settings>config vars
Copy NEW_RELIC_LICENSE_KEY from Heroku Settings to newrelic.js

_If hosting elsewhere, still add New Relic here_

Configure Availability Monitoring Alerts to look for a specific string
Configure Availability Monitoring Alerts to email Lead Engineer, and PMO

### Integrations
Configure New Relic to integrate into PivotalTracker, so issues can be pushed from the monitoring solution to Pivotal

Configure Bitbucket to link to Pivotal

Add Slack Web Hook to Pivotal Tracker
Add Slack POST Hook to BitBucket

Recommended Pivotal Integration Settings:
```
 Added feature
 Added comment
 Started feature
 Finished feature
 Accepted feature
 Rejected feature

 Paste to #projectname-dev channel
``` 



add Heroku git remote ( qa for qa, heroku for production )
- `git remote add qa git@bitbucket.org:rokk3rlabs/....`


## Populating Brain Documentation

Answer all the questions in `docs/**/*.md`

Fill out `meta/*.md`

Add new files / folders as needed. Remove freely.

Contribute good documentation and code back to the hivemind. r3l-node-stack

## Work

**Map Feature List to Test Stubs**

**Tech Spec - Make API Spreadsheet**

**Tech Spec - Make Endpoints**

**Build out Controllers & Stub Functions**

**Wire up routes.js**

**Make Models with Stubs**

**Make Services with Stubs**

**Make generators for dummy data** (see)[r3l components/dummy.md]

**Hook up Landing Page to Mandrill** ( Use different accounts for Dev and Prod )

**Mask Rackspace CDN asset URLs with assets.domain.com **(avoids spam flagging)

** Ensure Login and password lookup works **

**Replace 404 Page**

** Optimize images, fonts and other media assets **

**Make / Assign Pivotal Tickets for Milestones, Endpoints and Designs**
Milestones should match the project plan, and helps verify that dependencies are met.

### Project Objectives

** Make functional sandbox **
A single page with all forms on the site. Debug information here too.


## iOS
Implmenet Crashalytics
Invite team to Crashlytics

## Bookmarks to save
Basecamp

Pivotal

BitBucket

Tech Spec

Drive Folder

New Relic / Analytics

Designs / Wireframes


## Ongoing

Make sure tests execute, incomplete is ok, inline errors is not.


