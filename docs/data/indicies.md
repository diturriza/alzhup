/*
Title: Database Indexes
Sort: 1
*/
What fields should be indexed?
Provide in a format that makes it easy to implement.

```
db.memories.ensureIndex({
    "memoryBanks": 1,
}, { "name" : "Memory Lookup" });
```
