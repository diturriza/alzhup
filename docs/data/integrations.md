/*
Title: Integrations
Sort: 1
*/

## External API Integrations
Facebook for photos ( v? )
Weather (v1)

## Native Platform Integrations
Photos
Notifications
Geolocation
Contacts
Share Extension - Add memories from entire platform