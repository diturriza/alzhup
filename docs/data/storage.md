/*
Title: Storage
Sort: 1
*/

# Storage
(v1) Memories will primarily be stored on the device and updated from cloud
(v+) Memories will be optionally be stored on the device, with primary memories stored on the server and synched across devices.