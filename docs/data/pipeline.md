/*
Title: Pipeline
Sort: 1
*/

## Example

## Data Input Sources

Bulk importing should be supported via native device libraries. 

### Patient User
User Data = Patient
Caregiver / Provider Data = Contacts
Memories
ActivitySession
Tags - Person / Custom

### Caregiver Users
Contacts
Memories

### Admin (System)
Memory Banks - Used by activities to select memories.
Competancies - Scores activities 
Activities - Set memory banks and competancies

## Data Transformations
### Memories
Memories will have meta information, MemoryBanks, details, date, tags, added by Patients and Caregivers. This data is used when the system selects Activities, see [Algorithms](../code/algos.md).

## Data Transformations

Once the data is in our system, how do we normalize it for our application?

Are there any special considerations for different sorts of data?

What are the transformation / association pipelines?

( ie. Users have many Orders and one Address. 

CompanyRecord.name maps to Sales.buyer for Transactions
)

## Data Relations

Memories<-memoryBanks,people,custom
ActivitySession<-user,ActivitySessionResult
ActivitySessionResult<-activity,memory,session

## Data Analysis

### Activity Sessions
Session competancy data needs to be aggregated so we can conduct data analysis.

Data will be investigated on a per patient and per activity basis with an emphasis on competancy improvement over time.

## Data Output

What fields should be indexed?

What objects will be consumed by the client?

Does the application need to support exports?

## Data Storage

If you have a data product, where will you store the data?