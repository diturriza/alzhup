/*
Title: Updates
Sort: 1
*/

## Data sync

Patients and Caregivers update Memory data

Patient login - sync memories with server

## Bulk
Photos can be imported from Device
URLs with many images / videos / audio


Are there automatic updates the application needs to process?
Are there manual updates the application needs to process?
How does the application handle data updates?

How do we anticipate data changing structure over time?
Are there any risk areas with populating data early vs later?
When / how will the seed data, if any, be populated? 

How do you anticipate the data growing? Will records themselves grow?
Where might growth be a problem?

