/*
Title: Sync 
Sort: 1
*/

# Sync

## Device Level
All memories should be stored on the device for offline access.

## Caregiver Website
Uploading memories to the website causes them to be synced to the device. 

## Cloud
All memories are saved in the cloud, and can be restored if needed.
