'use strict';

/**
 * Welcome to r3l Gulp
 * If you make a change here, please update the docs at intro/dev.md
 *
 */

var conf = require('./gulp.conf.js');

var gulp = require('gulp');
var mocha = require('gulp-mocha');
var gutil = require('gulp-util');
var less = require('gulp-less');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var tap = require('gulp-tap');
var log = require('captains-log')();
var chalk = require('chalk');
var nodemon = require('gulp-nodemon');
var openInChrome = require('gulp-open');
var clean = require('gulp-clean');
var replace = require('gulp-replace');
var fixme = require('fixme');
var shell = require('gulp-shell');

var sailsApp;
// Not used yet
// var uglify = require('gulp-uglify');
// var size = require('gulp-filesize');

/** Helper Functions */

/** Uses node file resource to retrieve relative path. for headers */
function getFileLoc(file){
  return require('path').relative( './', file.path);
}

/** Execute a test */
function doTest( files, cb ){
  gulp.src( files, {read: false})
  .pipe(mocha({
    // write xunit.xml for jenkins
    reporter: ( process.env.NODE_ENV === 'jenkins' ) ? 'xunit-file' : 'spec',
    timeout: 10000,
    globals: {
      should: require('should'),
      _: require('lodash')
    }
  }))
  .on('error', gutil.log)
  .on('end', cb);
}

gulp.task('install', shell.task(['npm install', 'bower install']));

// =========== SERVER ==============
// gulp server - runs build and a server, opens browser window
// gulp server:restart - lowers and raises a server, used internally
// gulp server:api - start stub server for prototyping

gulp.task('server', ['build'], function(cb){
  var SailsApp = require('sails').Sails;
  var sails = new SailsApp();
  sails.lift({ hooks: { grunt: false }},
    function(err, app){
      sailsApp = app;
      log(app);
      log('==> CODE HAPPY @ r3l <==');
      gulp.src('./gulpfile.js')
      .pipe(openInChrome('', {url:'http://localhost:1337'}));
      cb();
    });
});

gulp.task('server:prod', shell.task('NODE_ENV=production PORT=80 gulp server'));
gulp.task('watch:prod', shell.task('NODE_ENV=production PORT=80 gulp server'));

gulp.task('server:restart', function(cb){
  sailsApp.lower( function() {
    setTimeout(
      function() {
        var SailsApp = require('sails').Sails;
        var sails = new SailsApp();
        sails.lift({ hooks: { grunt: false }},
          function(err, app){
            sailsApp = app;
            cb();
          });
      }, 100);
  });
});

gulp.task('server:lower', function(cb){
  sailsApp.lower(function(){
    cb();
  });
});

gulp.task('server:lift', function(cb) {
  var SailsApp = require('sails').Sails;
  var sails = new SailsApp();
  sails.lift({ hooks: { grunt: false }
    }, function(err, app) {
      sailsApp = app;
      cb();
    });
});


// Console -  don't use update to use gulp-run
gulp.task('console', shell.task(['sails console --verbose']));
gulp.task('console:stage', shell.task(['NODE_ENV=stage sails console --verbose']));
gulp.task('console:prod', shell.task(['NODE_ENV=production sails console --verbose']));


// ================= BUILD ==================
// gulp build - build all the things, copy to buildDirs.base
// gulp build:js - build the front end javascript
// gulp build:media - build the images/fonts/video/audio
// gulp build:html - build the html and views

gulp.task('build', ['build:js', 'build:css', 'build:media', 'build:html']);

/** Build LESS, put extra css into vendor.css */
gulp.task('build:css', ['build:css:vendor'], function(){
  return gulp.src( conf.lessFiles )
    // .pipe(size())
    .pipe(less())
    .pipe(rename(function(path){
      path.basename = 'styles';
    }))
    .pipe(gulp.dest(conf.buildDirs.css));
});

gulp.task('build:css:vendor', function(){
  return gulp.src( conf.cssVendorFiles )
  .pipe(concat(  conf.targetName.vendorCss ))
  .pipe( gulp.dest(conf.buildDirs.css));
});

/** Copy html and templates to public */
gulp.task('build:html', function(){
  // all about that base
  return gulp.src( conf.htmlFiles, {base: 'assets' })
  // version replace
    .pipe(replace('<!-- #insertVersion -->',
          JSON.parse(require('fs').readFileSync('package.json')).version))
    .pipe(gulp.dest( conf.buildDirs.base ));
});

/** Copy media files to /public */
gulp.task('build:media', function(){
  return gulp.src(conf.mediaFiles, { base: 'assets' })
    .pipe(gulp.dest( conf.buildDirs.base));
});

/** Builds app.js and vendor.js */
gulp.task('build:js', ['build:js:vendor', 'build:js:shims'], function(){
  return gulp.src( conf.ngCode )
  .pipe(tap(function(file){
    // add file info to headers
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( conf.targetName.js))
  .pipe(gulp.dest(conf.buildDirs.js));
});

gulp.task('build:js:vendor', function(){
  return gulp.src( conf.vendorScripts )
  .pipe(tap(function(file){
    // add file info to headers
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( conf.targetName.vendorJs))
  .pipe(gulp.dest(conf.buildDirs.js));
});

gulp.task('build:js:shims', function(){
  // copy shims
  return gulp.src( conf.shims )
  .pipe(gulp.dest(conf.buildDirs.shims));
});

// == Implementation Helpers
gulp.task('server:prod', shell.task('NODE_ENV=production gulp server'));
gulp.task('watch:prod', shell.task('NODE_ENV=production gulp server'));
gulp.task('server:stage', shell.task('NODE_ENV=stage gulp server'));
gulp.task('watch:stage', shell.task('NODE_ENV=stage gulp server'));


// ==== CODE QUALITY ------>

gulp.task('check', function(){
  return gulp.src( conf.workingCode )
  .pipe(jshint())
  .pipe(tap( function(file){
    if (file.jshint.success){
      log.info( chalk.green( getFileLoc(file), 'passed!\n' ));
    }
  }))
  .pipe(jshint.reporter(stylish));
});

gulp.task('todo', function() {
  fixme({
    ignored_directories: [
      '**/.grunt/**',
      'node_modules/**',
      '.git/**',
      '.tmp/**',
      'assets/vendor/**'
    ]
  })
});

// * DOCS
gulp.task('rundocs', ['copydocconfig', 'copydocs'], shell.task([
  'cd ./node_modules/raneto; rm -r public/images; mv content/images public/images; node ./bin/www; open http://localhost:3000;'
]));

gulp.task('copydocs', ['cleardocs'], function() {
  return gulp.src(['docs/**/*'], ['!docs/images']).pipe(gulp.dest('./node_modules/raneto/content/'));
});

gulp.task('copydocconfig', function() {
  var config = require('raneto/config');
  config.site_title = require('./package.json').name + " Documentation";
  config.authentication = true;
  config.credentials = { username: conf.docs.user, password: conf.docs.password };
  require('fs').writeFileSync('./node_modules/raneto/config.js', "module.exports=" + JSON.stringify(config));
});


gulp.task('cleardocs', function() {
  return gulp.src('./node_modules/raneto/content/*').pipe(clean());
});

gulp.task('docs', ['cleardocs', 'copydocs', 'copydocconfig', 'rundocs']);
gulp.task('watch:docs', function() {
  gulp.watch(['docs/**/*'], ['copydocs']);
});
// ===== MAINTAIN ----------- >

// empties .tmp
gulp.task('clean', function(){
  return gulp.src( conf.buildDirs.base, { read:false })
  .pipe(clean());
});


// ==== WATCH ====
// gulp watch - watches front end and back end code, restarts and rebuilds
// gulp watch:unit - use for building unit tests
// gulp watch:api - use for building api tests
// gulp watch:e2e - use for building e2e tests (TODO)
gulp.task('watch', ['build', 'server'], function(){
  log('Watching files for JS development');
  gulp.watch(conf.ngCode, ['build']);
  gulp.watch(conf.htmlFiles, ['build']);
  gulp.watch(conf.lessFiles, ['build']);
  gulp.watch(conf.apiCode, ['server:restart']);
  gulp.watch(conf.configCode, ['server:restart']);
});

// run node inspector
gulp.task('watch:debug', ['build'], function(){
  nodemon({
    script : 'app.js',
    env: { 'NODE_ENV': 'development' },
    ignore: ['!api/**/*.js', '!api/**/**/*.js'],
    nodeArgs: ['--debug']
  })
  .on('restart', function(){ log('restarted'); });
});

gulp.task('watch:unit', function(){
  gulp.watch(conf.testFiles.concat(conf.apiCode), ['test:unit']);
  log('Watching files for Unit testing');
});

gulp.task('watch:api', function(){
  gulp.watch(conf.testFiles.concat(conf.ngCode, conf.apiCode), ['test:api']);
  log('Watching files for API/E2E testing');
});

gulp.task('watch:test', function(){
  gulp.watch(conf.testFiles, ['test']);
  log('Watching files for testing... o\\|/o');
});

gulp.task('watch:e2e', ['watch:api']);


// ========== TEST ========
// these are mostly for internal use
// gulp test - run the tests


gulp.task('test', ['alltest:unit', 'alltest:api']);

gulp.task('alltest:unit', function(cb) {
  return doTest(conf.unitTestFiles, cb);
});

gulp.task('alltest:api', ['alltest:unit'], function(cb) {
  return doTest(conf.apiTestFiles, cb);
});

gulp.task('test:unit', function(cb) {
  return doTest(conf.unitTestFiles, cb);
});

gulp.task('test:api', function(cb) {
  return doTest(conf.apiTestFiles, cb);
});

// ======= DEBUG PATHS ---------- >

gulp.task('debug', [ 'debug:test:api' ]);
gulp.task('debug:server', function(){
  return gulp.src(conf.apiCode)
  .pipe(tap( function(file){
    log(getFileLoc(file));
  }));
});
gulp.task('debug:front', ['debug:server'], function(){
  return gulp.src(conf.ngCode)
  .pipe(tap( function(file){
    log(getFileLoc(file));
  }));
});
gulp.task('debug:test:unit', ['debug:front'], function(){
  return gulp.src(conf.unitTestFiles)
  .pipe(tap( function(file){
    log(getFileLoc(file));
  }));
});
gulp.task('debug:test:api', ['debug:test:unit'], function(){
  return gulp.src(conf.apiTestFiles)
  .pipe(tap( function(file){
    log(getFileLoc(file));
  }));
});

gulp.task('default', function(){
  console.log('\n>- ', chalk.blue('BUILD COMMANDS'),' -------------- \n');
  console.log( '       gulp watch ',
    chalk.gray('Builds, lifts, watches for changes, rebuilds') );
  console.log( ' gulp watch:debug ',
    chalk.gray('Builds, runs nodemon & node inspector') );
  console.log( '        gulp docs ',
    chalk.gray('Compile and Serve `/docs` on localhost:3000') );
  console.log( '      gulp server ',
    chalk.gray('Builds, lifts, opens browser') );
  console.log( '        gulp test ',
    chalk.gray('Runs all tests, each has own server lifecycle') );
  console.log( '    gulp test:api ',
    chalk.gray('Run just the api tests') );
  console.log( '   gulp test:unit ',
    chalk.gray('Run just the unit tests') );
  console.log( '       gulp build ',
    chalk.gray('Build all front end code') );
  console.log( '       gulp debug ',
    chalk.gray('Log all input file paths') );
  console.log( '       gulp check ',
    chalk.gray('Run jshint on all the things') );
  console.log('        gulp todo ',
    chalk.gray('TODO, OPTIMIZE, NOTE, HACK, XXX, FIXME, BUG comments'));
  console.log('gulp db:prod:local',
    chalk.gray('Backup from production db to local'));
  console.log('\n>--------------- \n');
  console.log(chalk.cyan('   code '), chalk.yellow('happy'),'\n');
  log('\n Configure file paths in gulp.conf.js');
});


//       ####  ##
//          #   #
// ###    ##    #
// #  #     #   #
// #     #  #   #
// #      ##   ###


gulp.task('banner', function(){
  require('./test/fixtures/banner.js')();
});

// ====== MIGRATIONS ----------->

// This is fucking awesome, make sure
// config.mongoUrl.production, config.dataBaseName are set

gulp.task('db:prod:local', function(){
  if ( config.mongoUrl.production === '' ) {
    return log('No Prod DB. Did you update config.mongoUrl.production?')
  }

  if ( config.databaseName === '' ){
    return log('No config.database')
  }

  log('Moving Production DB to Local. ^C to abort.');

  var prodMongoObj = config.connections.mongoUrlToParamObj(config.mongoUrl.production);

  return gulp.src('*.js').pipe(shell([
    'mongodump ' + config.connections.mongoObjToCommandLine(prodMongoObj) + ' -o dump',
    'mongorestore --db='+ config.databaseName +' --drop dump/'+prodMongoObj.database,
    'rm -r dump/'
    ]));
});

// TODO Image optimization
// TODO Add minification / uglification
