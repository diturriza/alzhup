/* jshint camelcase:false */

/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */

var config = require('./config/appConfig.js').appConfig;
var qaKey = config.newRelicKey.qa;
var prodKey = config.newRelicKey.prod;

exports.config = {
  /**
   * Array of application names.
   */
  app_name : [ config.appName +  ' - '  + ( process.env.NODE_ENV === 'production' ) ? 'production' : 'development' ],
  /**
   * Your New Relic license key.
   */
  license_key : ( process.env.NODE_ENV === 'production' ) ? prodKey : qaKey,
  logging : {
    /**
     * 'trace' is most useful to New Relic when diagnosing
     * 'info' and higher will impose the least overhead on
     * production applications.
     */
    level :  ( process.env.NODE_ENV === 'production' ) ? 'info' : 'trace'
  }
};
