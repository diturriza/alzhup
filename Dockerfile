FROM node:latest
MAINTAINER Sean Canton <sean@rokk3rlabs.com>

# Environment Variables
ENV MONGO_URL mongodb://r3l2user:r3l2Trudat55!@c870.candidate.13.mongolayer.com:10870,candidate.1.mongolayer.com:10235/r3l2?replicaSet=set-54ea0e29fd540f9266000208
ENV PORT 80

# Copy from Local to Docker
WORKDIR /src
ADD . /src
RUN rm -rf /src/.git

# Setup App
RUN npm install sails@latest gulp bower -g
RUN npm install forever -g

# Allow build
RUN chmod -R 777 /src
RUN npm install

# Port 80
EXPOSE 80

# Run
RUN bower install --allow-root && gulp clean && gulp build
CMD ["node", "server"]

# Run docker
# docker build -t brianofrokk3r/r3l2 .
# docker run -t -i -d -p 80:80 brianofrokk3r/r3l2