'use strict';

app.controller('ForgotPasswordCtrl', function($scope, $http, $location, $rootScope, AuthService) {
  $scope.submit = function() {
    $http.post('/forgot-password', $scope.loginModel)
      .success(function(data) {
        $location.path('/');
      })
      .error(function(err) {
        window.alert(err);
      });
  }
});