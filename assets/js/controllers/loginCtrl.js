﻿'use strict';

app.controller('LoginCtrl', function($scope, $http, $location, $rootScope, AuthService) {
  $scope.submit = function() {
    $http.post('/login', $scope.loginModel)
      .success(function(data) {
        $rootScope.currentUser = data.results;
        window.localStorage.setItem("user", JSON.stringify($rootScope.currentUser));
        $location.path('/dashboard');
      })
      .error(function(err) {
        window.alert(err);
      });
  }
});