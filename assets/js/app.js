var app = angular.module('r3lApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'ngAnimate',
    'mgcrea.ngStrap'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    
    //use angular-ui-router to manage routing
    $stateProvider
      .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'views/login.html',
        data: {
          requireLogin: false // this property will apply to all children of 'app'
        }
      })
      .state('join', {
        url: '/join',
        controller: 'JoinCtrl',
        templateUrl: 'views/join.html',
        data: {
          requireLogin: false // this property will apply to all children of 'app'
        }
      })
      .state('home', {
        url: '/home',
        controller: 'HomeCtrl',
        templateUrl: 'views/home.html',
        data: {
          requireLogin: false // this property will apply to all children of 'app'
        }
      })
      .state('forgot-password', {
        url: '/forgot-password',
        controller: 'ForgotPasswordCtrl',
        templateUrl: 'views/forgotpassword.html',
        data: {
          requireLogin: false // this property will apply to all children of 'app'
        }
      })
      .state('app', {
        abstract: true,
        data: {
          requireLogin: true // this property will apply to all children of 'app'
        },
        template: '<ui-view/>'
      })
      .state('app.dashboard', {
        url: '/dashboard',
        controller: 'DashboardCtrl',
        templateUrl: 'views/dashboard.html'
      });

    $urlRouterProvider.otherwise('/login');

  })
  .run(function($rootScope, $location, AuthService, $http) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
      var requireLogin = toState.data.requireLogin;
      if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
        $location.path('/login');
        // get me a login modal!
      }
    });
  });

// make jquery and lodash available
app.value('$', '$');
app.value('_', '_');