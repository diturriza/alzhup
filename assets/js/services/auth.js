﻿'use strict';
app.factory('AuthService', function Auth($location, $rootScope, $cookieStore, $http, $q) {
  // manage auth

  if($cookieStore.get('user')) {
    $rootScope.currentUser = $cookieStore.get('user');
  } else if(window.localStorage.getItem("user")) {
    $rootScope.currentUser = JSON.parse(window.localStorage.getItem("user"));
  } else {
    $rootScope.currentUser = null;
  }
  
  return {
    isLoggedIn: function() {
      var user = $rootScope.currentUser;
      return !!user;
    },
    logout: function() {
      $http.post('/logout').success(function(resp) {
        delete $rootScope.currentUser;
        $cookieStore.remove('user'); 
        window.localStorage.removeItem("user");
        $location.path('/login');
      });
    }
  }

})