var _ = require('lodash');

var buildDirs = {
  base: '.tmp/public/'
};

_.extend(buildDirs, {
  js: buildDirs.base + 'js/',
  css: buildDirs.base + 'css/',
  shims: buildDirs.base + 'shims/'
});

var targetName = {
  js: 'app.js',
  css: 'styles.css',
  vendorCss: 'vendor.css',
  vendorJs: 'vendor.js'
};

var baseNames = {
  css: 'styles',
  html: 'assets'

}

var docs = {
  user: 'docs',
  password: 'icap2015'
}

// [@css] - turn less into css
var lessFiles = 'assets/styles/less/app.less';

// [@css] - use `cssVendorFiles` to update your vendor.css files with things like bootstrap.css, etc...
var cssVendorFiles = [
  'assets/vendor/bootstrap/dist/css/bootstrap.css'
];

var cssTarget = 'assets/styles/css/';
var htmlFiles = ['assets/*.html', 'assets/views/*.html', 'assets/views/**/*.html', 'assets/favicon.ico', 'assets/robots.txt'];

// [@js] - build these into app.js and vendor.js
var sailsScripts = ['api/**/*.js'];
var jsScripts = ['assets/js/**/*.js', 'assets/js/**/**/*.js'];
var vendorScripts =
  [
    'assets/vendor/sails.io.js/dist/sails.io.js',

    'assets/vendor/jquery/dist/jquery.js',

    'assets/vendor/angular/angular.js',
    'assets/vendor/angular-resource/angular-resource.js',
    'assets/vendor/angular-cookies/angular-cookies.js',
    'assets/vendor/angular-sanitize/angular-sanitize.js',
    'assets/vendor/angular-route/angular-route.js',
    'assets/vendor/angular-bootstrap/ui-bootstrap-tpls.js',
    'assets/vendor/angular-ui-router/release/angular-ui-router.js',
    'assets/vendor/angular-animate/angular-animate.js',
    'assets/vendor/angular-strap/dist/angular-strap.min.js',

    'assets/vendor/bootstrap/dist/js/bootstrap.js',

    'assets/vendor/lodash/dist/lodash.js'
  ];

var shims = [
  'assets/vendor/es5-shim/es5-shim.js',
  'assets/vendor/json3/lib/json3.min.js'
];

var mediaFiles = ['assets/fonts/*', 'assets/images/*', 'assets/music/*', 'assets/videos/*'];


var fixtureFiles = ['test/fixtures/*.js'];
var unitTestFiles = ['test/unit.test.js', 'test/unit/**/*.js'];
var apiTestFiles = ['test/api.test.js', 'test/api/*.js'];
// all tests
var testFiles = unitTestFiles.concat(apiTestFiles, fixtureFiles);

//sails
var apiCode = ['api/**/*.js'];
var configCode = ['config/**/*.js'];
//angular
var ngCode = ['assets/js/**/*.js'];
// all production code
var self = ['gulp*.js'];
var workingCode = apiCode.concat(ngCode, self);
var allCode = workingCode.concat(testFiles);

var clientFiles = ["assets/*"];

module.exports = {
  clientFiles: clientFiles,
  buildDirs: buildDirs,
  targetName: targetName,
  lessFiles: lessFiles,
  cssVendorFiles: cssVendorFiles,
  cssTarget: cssTarget,
  htmlFiles: htmlFiles,
  vendorScripts: vendorScripts,
  shims: shims,
  mediaFiles: mediaFiles,
  unitTestFiles: unitTestFiles,
  apiTestFiles: apiTestFiles,
  testFiles: testFiles,
  apiCode: apiCode,
  ngCode: ngCode,
  workingCode: workingCode,
  // api, assets, tests
  allCode: allCode,
  configCode: configCode,
  docs:docs
};
