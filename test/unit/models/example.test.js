// /* global describe */
// /* global it */

// // [@test] - an example of how to lay this out
// describe.skip('Model Example', function() {
//   describe('crud', function() {

//     // These are really Waterline tests, but they demonstrate
//     // syntax and process

//     it('should create a example', function(done) {
//       Example.create(f.example, done);
//     });

//     it('should read a example', function(done) {
//       Example.findOne(f.example.id)
//       .then(function(results) {
//         results.foo.should.eql(f.example.foo);
//         done();
//       }).catch(done);
//     });

//     it('should update a example', function(done) {
//       Example.update(f.example.id, {
//         foo: f.answers.foo2
//       })
//       .then(function(results) {
//         results.foo.should.eql(f.answers.foo2);
//         done();
//       }).catch(done);
//     });
//   });

//   describe('associations', function() {

//     it('should create an associated Model', function(done) {
//       Example.findOne(f.example.id)
//       .populate('bar')
//       .then(function(results) {
//         results.bar = f.example.bar;
//         results.save(done);
//       }).catch(done);
//     });

//     it('should get an associated Model', function(done) {
//       Example.findOne(f.example.id)
//       .populate('bar')
//       .then(function(results) {
//         results.bar.should.eql(f.example.bar);
//         done();
//       }).catch(done);
//     });
//   });

//   describe('instance methods()', function() {

//     it('should this.fooBar()', function(done) {
//       Example.findOne(f.example.id)
//       .then(function(results) {
//         results.fooBar().should.eql(f.examples.foobar);
//         done();
//       }).catch(done);
//     });

//   });

//   describe('model methods()', function() {

//     it('should Example.fooBar()', function(done) {
//       Example.fooBar().should.eql(f.examples.foobar);
//       done();
//     });
//   });
// });