/* global describe */
/* global it */

// [@test] - tag Test
describe('Model Tag', function() {
  describe('crud', function() {

    it('should create a tag', function(done) {
      Tag.create(f.tag)
        .then(function(tag) {
          tag.name.should.eql(f.tag.name);
          f.tag.id = tag.id;
          done();
        }).catch(done);
    });

    it('should read a tag by name', function(done) {
      Tag.findOne({
        id: f.tag.id
      })
        .then(function(tag) {
          tag.name.should.eql(f.tag.name);
          done();
        }).catch(done);
    });

    it('should update a tag', function(done) {
      Tag.update({
        id: f.tag.id
      }, {
        name: f.tag.name
      })
        .then(function(tag) {
          tag[0].name.should.eql(f.tag.name);
          done();
        }).catch(function(err) {
          if (err) done();
        });
    });
  });
});