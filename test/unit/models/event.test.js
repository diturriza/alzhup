/* global describe */
/* global it */

// [@test] - event Test
describe('Model Event', function() {
  describe('crud', function() {

    it('should create an event', function(done) {
      Event.create(f.event)
        .then(function(event) {
          event.contactId.should.eql(f.event.contactId);
          done();
        }).catch(done);
    });

    it('should read an event by contactId', function(done) {
      Event.findOne({
        contactId: f.event.contactId
      })
        .then(function(event) {
          event.contactId.should.eql(f.event.contactId);
          done();
        }).catch(done);
    });

    it('should update an event', function(done) {
      Event.update({
        contactId: f.event.contactId
      }, {
        contactId: f.event.contactId,
      })
        .then(function(event) {
          event[0].contactId.should.eql(f.event.contactId);
          done();
        }).catch(done);
    });
  });
});