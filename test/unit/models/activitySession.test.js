/* global describe */
/* global it */

// [@test] - ActivitySession Test
describe('Model ActivitySession', function() {
  describe('crud', function() {

    it('should create an ActivitySession', function(done) {
      ActivitySession.create(f.activitySession)
        .then(function(ActivitySession) {
          ActivitySession.userId.should.eql(f.activitySession.userId);
          done();
        }).catch(done);
    });

    it('should read an ActivitySession by userId', function(done) {
      ActivitySession.findOne({
        userId: f.activitySession.userId
      })
        .then(function(ActivitySession) {
          ActivitySession.userId.should.eql(f.activitySession.userId);
          done();
        }).catch(done);
    });

    it('should update an ActivitySession', function(done) {
      ActivitySession.update({
        userId: f.activitySession.userId
      }, {
        userId: f.activitySession.userId,
      })
        .then(function(ActivitySession) {
          ActivitySession[0].userId.should.eql(f.activitySession.userId);
          done();
        }).catch(done);
    });
  });
});