/* global describe */
/* global it */

// [@test] - message Test
describe('Model Message', function() {
  describe('crud', function() {

    it('should create a message', function(done) {
      Message.create(f.message)
        .then(function(message) {
          message.senderContactId.should.eql(f.message.senderContactId);
          done();
        }).catch(done);
    });

    it('should read a message by senderContactId', function(done) {
      Message.findOne({
        senderContactId: f.message.senderContactId
      })
        .then(function(message) {
          message.senderContactId.should.eql(f.message.senderContactId);
          done();
        }).catch(done);
    });

    it('should update a message', function(done) {
      Message.update({
        senderContactId: f.message.senderContactId
      }, {
        senderContactId: f.message.senderContactId,
      })
        .then(function(message) {
          message[0].senderContactId.should.eql(f.message.senderContactId);
          done();
        }).catch(done);
    });
  });
});