/* global describe */
/* global it */

// [@test] - score Test
describe('Model Score', function() {
  describe('crud', function() {

    it('should create a score', function(done) {
      Scores.create(f.score)
        .then(function(score) {
          score.competancy.should.eql(f.score.competancy);
          done();
        }).catch(done);
    });

    it('should read a score by competancy', function(done) {
      Scores.findOne({
        competancy: f.score.competancy
      })
        .then(function(score) {
          score.competancy.should.eql(f.score.competancy);
          done();
        }).catch(done);
    });

    it('should update a score', function(done) {
      Scores.update({
        competancy: f.score.competancy
      }, {
        competancy: f.score.competancy,
      })
        .then(function(score) {
          score[0].competancy.should.eql(f.score.competancy);
          done();
        }).catch(done);
    });
  });
});