/* global describe */
/* global it */

// [@test] - Competancy Test
describe('Model Competancy', function() {
  describe('crud', function() {

    it('should create a competancy', function(done) {
      Competancy.create(f.competancy)
        .then(function(competancy) {
          competancy.name.should.eql(f.competancy.name);
          done();
        }).catch(done);
    });

    it('should read a competancy by name', function(done) {
      Competancy.findOne({
        name: f.competancy.name
      })
        .then(function(competancy) {
          competancy.name.should.eql(f.competancy.name);
          done();
        }).catch(done);
    });

    it('should update a competancy', function(done) {
      Competancy.update({
        name: f.competancy.name
      }, {
        name: f.competancy.name,
      })
        .then(function(competancy) {
          competancy[0].name.should.eql(f.competancy.name);
          done();
        }).catch(done);
    });
  });
});