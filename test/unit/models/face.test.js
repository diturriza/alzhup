/* global describe */
/* global it */

// [@test] - face Test
describe('Model Face', function() {
  describe('crud', function() {

    it('should create a face', function(done) {
      Face.create(f.face)
        .then(function(face) {
          face.personTagId.should.eql(f.face.personTagId);
          done();
        }).catch(done);
    });

    it('should read a face by personTagId', function(done) {
      Face.findOne({
        personTagId: f.face.personTagId
      })
        .then(function(face) {
          face.personTagId.should.eql(f.face.personTagId);
          done();
        }).catch(done);
    });

    it('should update a face', function(done) {
      Face.update({
        personTagId: f.face.personTagId
      }, {
        personTagId: f.face.personTagId,
      })
        .then(function(face) {
          face[0].personTagId.should.eql(f.face.personTagId);
          done();
        }).catch(done);
    });
  });
});