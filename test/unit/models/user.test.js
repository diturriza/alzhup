/* global describe */
/* global it */

// [@test] - User Test
describe('Model User', function() {
  describe('crud', function() {

    it('should create an user', function(done) {
      f.user.username = "AdminTest";
      User.create(f.user)
        .then(function(user) {
          user.username.should.eql(f.user.username);
          f.user.id = user.id;
          done();
        }).catch(done);
    });

    it('should read an user by username', function(done) {
      User.findOne({
        id: f.user.id
      })
        .then(function(user) {
          user.username.should.eql(f.user.username);
          done();
        }).catch(done);
    });

    it('should update an user', function(done) {
      User.update({
        id: f.user.id
      }, {
        username: f.user.username,
        password: f.user.password
      })
        .then(function(user) {
          user[0].username.should.eql(f.user.username);
          done();
        }).catch(function(err) {
          if (err)
            done();
        });
    });
  });
});