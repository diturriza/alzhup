/* global describe */
/* global it */

// [@test] - Contact Test
describe('Model Contact', function() {
  describe('crud', function() {

    it('should create a contact', function(done) {
      Contact.create(f.contact )
      .then(function (contact){
        contact.name.should.eql(f.contact.name);
        f.contact.id = contact.id;
        done();
      }).catch(done);
    });

    it('should read  contact by name', function(done) {
      Contact.findOne({
        id: f.contact.id
      })
        .then(function(contact) {
          contact.name.should.eql(f.contact.name);
          done();
        }).catch(done);
    });

    it('should update a contact', function(done) {
      Contact.update({
        id: f.contact.id
      }, {
        phone: f.contact.phone
      })
        .then(function(contact) {
          contact[0].name.should.eql(f.contact.name);
          done();
        }).catch(function (err) {
          if(err) log(err);
          done();
        });
    });
  });
});