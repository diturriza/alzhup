/* global describe */
/* global it */

// [@test] - memoryBank Test
describe('Model memoryBank', function() {
  describe('crud', function() {

    it('should create a memoryBank', function(done) {
      MemoryBank.create(f.memoryBank)
        .then(function(memoryBank) {
          memoryBank.name.should.eql(f.memoryBank.name);
          done();
        }).catch(done);
    });

    it('should read a memoryBank by name', function(done) {
      MemoryBank.findOne({
        name: f.memoryBank.name
      })
        .then(function(memoryBank) {
          memoryBank.name.should.eql(f.memoryBank.name);
          done();
        }).catch(done);
    });

    it('should update a memoryBank', function(done) {
      MemoryBank.update({
        name: f.memoryBank.name
      }, {
        name: f.memoryBank.name,
      })
        .then(function(memoryBank) {
          memoryBank[0].name.should.eql(f.memoryBank.name);
          done();
        }).catch(done);
    });
  });
});