/* global describe */
/* global it */

// [@test] - Activity Test
describe('Model Activity', function() {
  describe('crud', function() {

    it('should create an Activity', function(done) {
      Activity.create(f.activity)
        .then(function(Activity) {
          Activity.gameId.should.eql(f.activity.gameId);
          done();
        }).catch(done);
    });

    it('should read an Activity by gameId', function(done) {
      Activity.findOne({
        gameId: f.activity.gameId
      })
        .then(function(Activity) {
          Activity.gameId.should.eql(f.activity.gameId);
          done();
        }).catch(done);
    });

    it('should update an Activity', function(done) {
      Activity.update({
        gameId: f.activity.gameId
      }, {
        gameId: f.activity.gameId,
      })
        .then(function(Activity) {
          Activity[0].gameId.should.eql(f.activity.gameId);
          done();
        }).catch(done);
    });
  });
});