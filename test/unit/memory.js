/*

( API )
User can retrieve a memory
User can add a memory
User can edit a memory
User cannot edit another's memory
User can delete a memory
User cannot delete another's memory

Memories can be tagged with system tags
Memories can be tagged with people tags
Memories can be tagged with custom tags
Memories can have tags removed

Memories can be retrieved by system tags
Memories can be retrieved by people tags
Memories can be retrieved by custom tags

 */