/* global describe */
/* global it */

// [@test] - Memories Test
describe('My Memories', function() {
  describe('features', function() {
    before(function() {
      // Mocking our service

    });

    after(function() {

      // Restores our mock to the original service

    });

    it('should add photo', function(done) {
      done();
    });

    it('should add video', function(done) {
      done();
    });

    it('should set tag Memories with memory bank tags', function(done) {
      done();
    });

    it('should get Memories', function(done) {
      done();
    });
    it('should import', function(done) {
      done();
    });
    it('should get timeline', function(done) {
      done();
    });
    it('should get modal with inline editing', function(done) {
      done();
    });
  });
});