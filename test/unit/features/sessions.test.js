/* global describe */
/* global it */

// [@test] - Sessions Test
describe('Sessions', function() {
  describe('features', function() {
    before(function() {
      // Mocking our service

    });

    after(function() {

      // Restores our mock to the original service

    });

    it('should Start Session', function(done) {
      done();
    });
    it('should Finish Session', function(done) {
      done();
    });
    it('should Start Activity', function(done) {
      done();
    });
    it('should Select next Activity', function(done) {
      done();
    });
    it('should get Schedule Activity', function(done) {
      done();
    });
    it('should Get Session Details page', function(done) {
      done();
    });
    it('should set Session timeout', function(done) {
      done();
    });
  });
});