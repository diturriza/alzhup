var Sails = require('sails');


before(function(done) {

  console.log('==================================');
  console.log('=|  _______  _____  _____  r3l  |=');
  console.log('=|  |_____| |_____]   |    test |=');
  console.log('=|  |     | |       __|__       |=');
  console.log('==================================');

  Sails.lift({
    connections: {
      mongo: {
        database: 'alzhup-testing'
      }
    },
    models: {
      migrate: 'drop'
    },
    log: {
      level: 'debug'
    },
    hooks: {
      grunt: false,
      i18n: false
    }
  }, function(err, sails){
    Sails = sails;


    request = require('supertest');
    request = request('http://localhost:1337');
  // //clear console
  // log('\033[2J');

  f = {};
  // fs localized to root
  var files = require('fs').readdirSync('./test/fixtures');
  files.forEach(function(file) {
    // require localized to this file
    f[file.slice(0, -3)] = require('./fixtures/' + file);
  });

  auth = require('./fixtures/_auth.js');
  
  // populate database with fixture data
  f._generate.testData(f, function(err){

    if (err) console.error(err);
    log('Import Data Done...')
    done(err, sails);
  });

  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  Sails.lower(done);
});