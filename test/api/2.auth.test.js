'use strict';
/* jshint undef: false */
/* jshint unused: false */

describe('Auth Test', function() {
  it('it must to authenticate via oAuth', function(done) {
    request
      .post('/oauth/access_token')
      .send(f._login)
      .expect(200)
      .expect(requestKeys)
      .end(function(err, res) {
        if (err) {
          log(err);
          return done(err);
        }
        done();
      });

    function requestKeys(res) {
      if (!('access_token' in res.body)) return 'Missing oAuth token';
    }
  });
});