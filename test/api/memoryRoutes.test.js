describe('Memory Routes', function() {

  var cookie;

  before(function(done) {
    auth.getCookie(function(err, result) {
      if (err) return done(err);
      cookie = result;
      done();
    });
  });

  after(function(done) {
    auth.logout(cookie,function(err) {
      if (err) return done(err);
      done();
    });
  });  

  describe('POST', function() {
    describe('/memory', function() {
      it('should create a new memory', function(done) {

        request.post('/memory')
          .set('Accept', 'application/json')
          .set('cookie', cookie)
          .expect('Content-Type', /json/)
          .send(f.memory)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'resourceUrl')) {
              f.memory.id = res.body.id;
              done();
            }
          });
      });
    });
    describe('/memory/:id/upload', function() {
      it('should upload a file', function(done) {

        request.post('/memory/'+f.memory.id+'/upload')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .attach('uploadFile', './test/files/alzhup.png')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message'))
              done();
          });
      });
    });
  });

  describe('GET', function() {
    describe('/memory/:id', function() {
      it('should get some memory by id', function(done) {

        request.get('/memory/' + f.memory.id)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'resourceUrl')) {
              done();
            }
          });
      });
    });

    describe('/memory/find/:tagType/:tagId', function() {
      it('should get Memory by tag Id', function(done) {
        var url = '/memory/find/' + f.tag.type + '/' + f.tag.id;
        request.get(url)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/find/bank/:bankId', function() {
      it('should get memory by bank id', function(done) {

        request.get('/memory/find/bank/' + f.memoryBank.id)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/list/:startDate/:limit/:skip', function() {
      it('should get memory since some day', function(done) {

        request.get('/memory/list/2015-05-05/' + 10 + '/' + 1)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/featured', function() {
      it('should get featured memory', function(done) {

        request.get('/memory/featured')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });
  });

  describe('DELETE', function() {
    describe('/memory/:id/tag/:tagId', function() {
      it('should unlink tag', function(done) {

        request.del('/memory/' + f.memory.id + '/tag/' + f.tag.id)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/:id/face/:personTagId', function() {
      it('should unlink face', function(done) {

        request.del('/memory/' + f.memory.id + '/face/' + f.face.id)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/:id', function() {
      it('should del a memory', function(done) {

        request.del('/memory/' + f.memory.id)
          .send(f.memory)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });
  });

  describe('PUT', function() {
    describe('/memory/:id/details', function() {
      it('should update Memory Details by id', function(done) {

        request.put('/memory/' + f.memory.id + '/details')
          .send(f.memory)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/:id/date', function() {
      it('should update memory date', function(done) {

        request.put('/memory/' + f.memory.id + '/date')
          .send(f.memory)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/:id/tag/:tagId', function() {
      it('should link tag ', function(done) {

        request.put('/memory/' + f.memory.id + '/tag/' + f.tag.id)
          .send(f.memory)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });

    describe('/memory/:id/face/:personTagId/:topLeft/:bottomRight', function() {
      it('should link face ', function(done) {

        request.put('/memory/' + f.memory.id + '/face/' + f.face.id + '/:topLeft/:bottomRight')
          .send(f.memory)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
    });
  });
});