describe('Activity Routes', function() {

  var cookie;

  before(function(done) {
    auth.getCookie(function(err, result) {
      if (err) return done(err);
      cookie = result;
      done();
    });
  });

  after(function(done) {
    auth.logout(cookie,function(err) {
      if (err) return done(err);
      done();
    });
  });  

  describe('POST', function() {
    describe('/activity', function() {
      it('should create a new activity', function(done) {
        request
          .post('/activity')
          .set('cookie', cookie)
          .send(f.activity)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'gameId')) {
              f.activity.id = res.body.id;
              done();
            }
          });
      });
    });
  });
  describe('PUT', function() {
    describe('/activity/:id', function() {
      it('should update an activity', function(done) {
        request
          .put('/activity/' + f.activity.id)
          .set('cookie', cookie)
          .send(f.activity)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'gameId')) {
              done();
            }
          });
      });
    });
  });
  describe('GET', function() {
    describe('/activity/next', function() {
      it('should get Next Activity', function(done) {
        request.get('/activity/next')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);

            if (_.has(res.body, 'gameId')) {
              done();
            }
          });
      });
    });


    describe('/activity/list', function() {
      it('should get all activity', function(done) {

        request.get('/activity/list')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'id')) {
              done();
            }
          });
      });
    });

    describe('/activity/:id', function() {
      it('should get an activity by id', function(done) {
        request.get('/activity/' + f.activity.id)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'id')) {
              done();
            }
          });
      });
    });
  });
  describe('DELETE', function() {
    describe('/activity/:id', function() {
      it('should delete an activity by id', function(done) {
        request.del('/activity/' + f.activity.id)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message')) {
              done();
            }
          });
      });
    });
  });
});