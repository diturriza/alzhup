describe('Event Routes', function() {

  describe('POST', function() {
    describe('/event', function() {
      it('should create a new event', function(done) {

        request.post('/event')
          .expect('Content-Type', /json/)
          .send(f.event)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'contactId')) {
              f.event.id = res.body.id;
              done();
            }
          });
      });
    });
  });

  describe('PUT', function() {
    describe('/event/:id', function() {
      it('should update an event', function(done) {

        request.put('/event/' + f.event.id)
          .send(f.event)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'contactId'))
              done();
          });
      });
    });
  });
  describe('GET', function() {
    describe('/event/:id', function() {
      it('should get event by id', function(done) {

        request.get('/event/' + f.event.id)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'contactId'))
              done();
          });
      });
    });

    describe('/event/list/:startDate/:numDays', function() {
      it('should get all event since start date and just certain number of days', function(done) {
        var date = f.event.eventTime;
        request.get('/event/list/' + date.toDateString() + '/5')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'contactId'))
              done();
          });
      });
    });
  });
  describe('DELETE', function() {
    describe('/event/:id', function() {
      it('should delete an event by id', function(done) {

        request.get('/event/' + f.event.id)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'contactId'))
              done();
          });
      });
    });
  });
});