describe('User Routes', function() {

  var cookie;

  before(function(done) {
    auth.getCookie(function(err, result) {
      if (err) return done(err);
      cookie = result;
      done();
    });
  });

  after(function(done) {
    auth.logout(cookie,function(err) {
      if (err) return done(err);
      done();
    });
  });  

  describe('POST', function() {
    describe('/user', function() {
      it('should create a new user', function(done) {
        f.user.username = "Admin2";
        request.post('/user')
          .set('Accept', 'application/json')
          .set('cookie', cookie)
          .expect('Content-Type', /json/)
          .send(f.user)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'username')) {
              f.user.id = res.body.id;;
              done();
            }
          });
      });
    });

    describe('/user/caregiver', function() {
      it('should create a new caregiver', function(done) {

        request.post('/user/caregiver')
          .set('cookie', cookie)
          .send(f.contact)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'name')) {
              f.contact.caregiverId = res.body.id;
              done();
            }
          });
      });
    });

    describe('/user/provider', function() {
      it('should create a new provider', function(done) {

        request.post('/user/provider')
          .set('cookie', cookie)
          .send(f.contact)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'name')) {
              f.contact.providerId = res.body.id;
              done();
            }
          });
      });
    });
  });
  describe('PUT', function() {
    describe('/user/:id', function() {
      it('should update an user', function(done) {
        request.put('/user/' + f.user.id)
          .set('cookie', cookie)
          .send(f.user)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'username')) {
              done();
            }
          });
      });
    });
  });
  describe('GET', function() {
    describe('/user/contacts', function() {
      it('should get all contacts', function(done) {
        request.get('/user/contacts')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'name')) {
              done();
            }
          });
      });
    });

    describe('/user/calendar', function() {
      it('should get all events', function(done) {
        request.get('/user/calendar')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'eventTime')) {
              done();
            }
          });
      });
    });

    describe('/user/messages', function() {
      it('should get all messages', function(done) {

        request.get('/user/messages')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'message')) {
              done();
            }
          });
      });
    });

    describe('/user/:id', function() {
      it('should get an user by id', function(done) {

        request.get('/user/' + f.user.id)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'username')) {
              done();
            }
          });
      });
    });
  });
  describe('DELETE', function() {
    describe('/user/caregiver/:id', function() {
      it('should delete a caregiver by id', function(done) {
        request.del('/user/caregiver/' + f.contact.caregiverId)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message')) {
              done();
            }
          });
      });
    });

    describe('/user/provider/:id', function() {
      it('should delete a provider by id', function(done) {
        request.del('/user/provider/' + f.contact.providerId)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message')) {
              done();
            }
          });
      });
    });
  });
});