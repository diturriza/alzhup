describe('Tag Routes', function() {

  var cookie;

  before(function(done) {
    auth.getCookie(function(err, result) {
      if (err) return done(err);
      cookie = result;
      done();
    });
  });

  after(function(done) {
    auth.logout(cookie,function(err) {
      if (err) return done(err);
      done();
    });
  });  

  describe('POST', function() {
    describe('/tag/custom', function() {
      it('should create a new custom tag', function(done) {

        request.post('/tag/custom')
          .set('Accept', 'application/json')
          .set('cookie', cookie)
          .expect('Content-Type', /json/)
          .send(f.tag)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'type')) {
              f.tag.customId = res.body.id;
              done();
            }
          });
      });
    });
    describe('/tag/people', function() {
      it('should create a new person tag', function(done) {

        request.post('/tag/people')
          .set('Accept', 'application/json')
          .set('cookie', cookie)
          .expect('Content-Type', /json/)
          .send(f.tag)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'type')) {
              f.tag.personId = res.body.id;
              done();
            }
          });
      });
    });
  });
  describe('GET', function() {
    describe('/tag/custom', function() {
      it('should get all custom tag', function(done) {

        request.get('/tag/custom')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'name')) {
              done();
            }
          });
      });
    });

    describe('/tag/people', function() {
      it('should get all person tag', function(done) {

        request.get('/tag/people')
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'name')) {
              done();
            }
          });
      });
    });
  });
  describe('DELETE', function() {
    describe('/tag/custom/:id', function() {
      it('should delete a custom tag', function(done) {
        var url = '/tag/custom/' + f.tag.customId;
        request.del(url)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message')) {
              done();
            }
          });
      });
    });
    describe('/tag/people/:id', function() {
      it('should delete a person tag', function(done) {
        var url = '/tag/people/' + f.tag.personId;
        request.del(url)
          .set('cookie', cookie)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message')) {
              done();
            }
          });
      });
    });
  });
});