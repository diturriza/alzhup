describe('System Routes', function() {

  var cookie;

  before(function(done) {
    auth.getCookie(function(err, result) {
      if (err) return done(err);
      cookie = result;
      done();
    });
  });

  after(function(done) {
    auth.logout(cookie,function(err) {
      if (err) return done(err);
      done();
    });
  });  

  describe('POST', function() {
    describe('/system/competancy', function() {
      it('should create a new Competancy', function(done) {

        request.post('/system/competancy')
          .set('Accept', 'application/json')
          .set('cookie', cookie)
          .expect('Content-Type', /json/)
          .send(f.competancy)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'name')) {
              done();
            }
          });
      });
    });
    describe('/system/bank', function() {
      it('should create a new memory bank', function(done) {

        request.post('/system/bank')
          .set('Accept', 'application/json')
          .set('cookie', cookie)
          .expect('Content-Type', /json/)
          .send(f.memoryBank)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'name')) {
              done();
            }
          });
      });
    });
  });
  describe('GET', function() {
    describe('/system/competancy', function() {
      it('should get all competancy', function(done) {

        request.get('/system/competancy')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'name')) {
              done();
            }
          });
      });
    });

    describe('/system/bank', function() {
      it('should get all memory bank', function(done) {

        request.get('/system/bank')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'name')) {
              done();
            }
          });
      });
    });
  });
});