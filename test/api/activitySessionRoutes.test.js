describe('Activity Session Routes', function() {

  describe('GET', function() {
    describe('/activity/session/start', function() {
      it('should start an Activity Session', function(done) {

        request.get('/activity/session/start')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'userId')) {
              done();
            }
          });
      });
    });
    describe('/activity/session/end', function() {
      it('should end an Activity Session', function(done) {

        request.get('/activity/session/end')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body[0], 'userId')) {
              done();
            }
          });
      });
    });
  });
  describe('POST', function() {
    describe('/activity/session/progress', function() {
      it('should post progress activity', function(done) {

        request.post('/activity/session/progress')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .send({
            activitySessionResults: []
          })
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            if (_.has(res.body, 'message'))
              done();
          });
      });
    });
  });
});