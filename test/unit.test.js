var Sails = require('sails');

before(function(done) {

  console.log('=======================================');
  console.log('=| _     _ __   _ _____ _______  r3l |=');
  console.log('=| |     | | \\  |   |      |    test |=');
  console.log('=| |_____| |  \\_| __|__    |         |=');
  console.log('=======================================');

  Sails.lift({
    connections: {
      mongo: {
        database: 'product-testing'
      }
    },
    models: {
      migrate: 'drop'
    },
    log: {
      level: 'debug'
    },
    hooks: {
      grunt: false,
      i18n: false
    }
  }, function(err, sails) {
    Sails = sails;
    // here you can load fixtures, etc.
    f = {};
    // fs localized to root
    var files = require('fs').readdirSync('./test/fixtures')
    files.forEach(function(file) {
      // require localized to this file
      f[file.slice(0, -3)] = require('./fixtures/' + file);
    });
    done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  Sails.lower(done);
});