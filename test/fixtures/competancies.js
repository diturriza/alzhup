module.exports = [{
  category: '1.1',
  text: 'Motivation'
}, {
  category: '1.2',
  text: 'Self-Control'
}, {
  category: '1.3',
  text: 'Volition'
}, {
  category: '1.4',
  text: 'Anosognosia'
}, {
  category: '1.5',
  text: 'Self-efficiency'
}, {
  category: '1.6',
  text: 'Positive Emotion'
}, {
  category: '1.7',
  text: 'Emotional Control'
}, {
  category: '2.1',
  text: 'Healthy habits'
}, {
  category: '2.2',
  text: 'Ventilational function'
}, {
  category: '2.3',
  text: 'Cardiovascular'
}, {
  category: '2.4',
  text: 'Motor Control'
}, {
  category: '2.5',
  text: 'Phychomotor Speed'
}, {
  category: '3.1',
  text: 'Visual perception'
}, {
  category: '3.2',
  text: 'Auditory perception'
}, {
  category: '3.3',
  text: 'Olfactory perception'
}, {
  category: '3.4',
  text: 'Tactile perception'
}, {
  category: '3.5',
  text: 'Arousal / Activation'
}, {
  category: '3.6',
  text: 'Change in orientation'
}, {
  category: '3.7',
  text: 'Selective attention'
}, {
  category: '3.8',
  text: 'Concentration'
}, {
  category: '3.9',
  text: 'Divide attention'
}, {
  category: '4.1',
  text: 'Episodic memory'
}, {
  category: '4.2',
  text: 'Semantic memory'
}, {
  category: '4.3',
  text: 'Procedime memory'
}, {
  category: '4.4',
  text: 'Autobiographic memory'
}, {
  category: '4.5',
  text: 'Prospective memory'
}, {
  category: '4.6',
  text: 'Metamemory'
}, {
  category: '4.7',
  text: 'Short-term memory coding'
}, {
  category: '4.8',
  text: 'Implicit memory'
}, {
  category: '4.9',
  text: 'Long-term memory recuperation'
}, {
  category: '4.10',
  text: 'Remote memory'
}, {
  category: '5.1',
  text: 'Executive function'
}, {
  category: '5.2',
  text: 'Work memory'
}, {
  category: '5.3',
  text: 'Spatial-temporal control'
}, {
  category: '5.4',
  text: 'Verbal fluidity'
}, {
  category: '5.5',
  text: 'Idatory Praxias'
}, {
  category: '5.6',
  text: 'Interest and activation'
}, {
  category: '5.7',
  text: 'Emotional control'
}, {
  category: '5.8',
  text: 'Preserverance / Imitation'
}, {
  category: '5.9',
  text: 'Reality orientation'
}, {
  category: '5.10',
  text: 'Calculation'
}, {
  category: '5.11',
  text: 'Oral comprehension'
}, {
  category: '5.12',
  text: 'Denomination'
}, {
  category: '5.13',
  text: 'Semantic fluidity'
}, {
  category: '5.14',
  text: 'Ideomotor praxia'
}, {
  category: '5.15',
  text: 'Motor writing'
}, {
  category: '5.16',
  text: 'Grammatical writing'
}, {
  category: '5.17',
  text: 'Reading'
}, {
  category: '5.18',
  text: 'Synthesis and association'
}, {
  category: '5.19',
  text: 'Phonologic fluidity and prosod'
}, {
  category: '5.20',
  text: 'Reasoning'
}, {
  category: '6.1',
  text: 'Reminiscence'
}, {
  category: '6.2',
  text: 'Relaxation / Anxiety'
}, {
  category: '6.3',
  text: 'Affectiveness / Emotional State'
}, {
  category: '6.4',
  text: 'Life history'
}]