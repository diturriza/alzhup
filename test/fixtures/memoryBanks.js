module.exports = [{
  category: '1.1',
  text: 'Who am I?'
}, {
  category: '1.2',
  text: 'Infancy'
}, {
  category: '1.3',
  text: 'Adolescence'
}, {
  category: '1.4',
  text: 'Youth'
}, {
  category: '1.5',
  text: 'Adulthood'
}, {
  category: '1.6',
  text: 'Me now'
}, {
  category: '1.7',
  text: 'Before and after'
}, {
  category: '2.1',
  text: 'Memory and moments'
}, {
  category: '2.2',
  text: 'Emotions'
}, {
  category: '2.3',
  text: 'Healthy Habits'
}, {
  category: '2.4',
  text: 'Bodily memories'
}, {
  category: '2.5',
  text: 'Environmental memories'
}, {
  category: '2.6',
  text: 'Collective memories'
}, {
  category: '2.7',
  text: 'Social norms'
}, {
  category: '2.8',
  text: 'Songs'
}, {
  category: '2.9',
  text: 'Sensorial memories'
}, {
  category: '2.10',
  text: 'Artistic Activities'
}, {
  category: '2.11',
  text: 'Skills'
}, {
  category: '3.1',
  text: 'Places'
}, {
  category: '3.2',
  text: 'People'
}, {
  category: '3.3',
  text: 'Objects'
}, {
  category: '3.4',
  text: 'Actions'
}]