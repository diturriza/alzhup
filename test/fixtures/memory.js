module.exports = {
  resourceUrl: 'http://placekitten.org/200/200',
  resourceType: 'photo',
  details: 'A photo of a cat',
  date: new Date()
}