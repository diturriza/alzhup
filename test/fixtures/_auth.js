'use strict';

module.exports = {
  getCookie: getCookie,
  logout: logout
};

function getCookie(cb) {
  request
    .post('/login')
    .send(f.user)
    .expect(200)
    .end(function(err, res) {
      if (err) return cb(err);
      var cookie = res.headers['set-cookie'];
      cb(null, cookie);
    });
}

function logout(cookie, cb) {
  request
    .post('/logout')
    .set('cookie', cookie)
    .expect(200)
    .end(function(err, res) {
      if (err) return cb(err);
      cb();
    });
}