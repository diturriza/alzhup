module.exports = {
  name: 'Alan',
  phone: '999 999-9999',
  address: 'Caracas',
  location: ['10.4653533', '-66.8881343,12'],
  relationship: 'Test relationship',
  isProvider: true,
  isCarigiver: false,
  isUser: false,
  details: 'Details Test',
  photoURL: 'http://ww1.prweb.com/prfiles/2013/10/19/11249836/1231435_625100317511896_1454519309_n.jpg',
  userId: ''
}