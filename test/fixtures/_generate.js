'use strict';

module.exports = {
  testData: testData
};

function testData(f, cb) {
  async.series([

      function(callback) {
        Activity.create(f.activity , function createdActivity(err, activity) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        ActivitySession.create(f.activitySession , function createActivitySession(err, activitySession) {
          if (err) callback(err);
          callback(null, activitySession);
        });
      },
      function(callback) {
        Competancy.create(f.competancy , function createdCompetancy(err, competancy) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Contact.create(f.contact , function createdContact(err, contact) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Event.create(f.event , function createdEvent(err, event) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Face.create(f.face , function createdFace(err, face) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Memory.create(f.memory , function createdMemory(err, memory) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        MemoryBank.create(f.memoryBank , function createdMemoryBank(err, memoryBank) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Message.create(f.message , function createdMessage(err, message) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Scores.create(f.score , function createdScore(err, score) {
          if (err) callback(err);
          callback();
        });
      },
      function(callback) {
        Tag.create(f.tag , function createdTag(err, tag) {
          if (err) callback(err);
          callback();
        });
      }
    ],
    function(err, results) {
      if (err) {
        //log(err);
        cb(err);
      }
      log('Data Generate Complete');
      cb();
    });
}